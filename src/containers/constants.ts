import fs from 'fs'
import { Container, interfaces } from 'inversify'
import path from 'path'
import { ContainerTypes } from '../helpers/ContainerValues'
import { environment, IEnvironment } from '../helpers/environment'

const constantsContainer = new Container()

constantsContainer.bind(ContainerTypes.STARTUP_TIME).toConstantValue(Date.now())
constantsContainer.bind(ContainerTypes.ENVIRONMENT).toConstantValue(environment)
constantsContainer.bind(ContainerTypes.TEMPLATES_DIR).toConstantValue(path.join(__dirname, '../templates'))
constantsContainer
  .bind<interfaces.Factory<string>>(ContainerTypes.TEMPLATE_PATH_FACTORY)
  .toFactory<string, [string]>((context) => {
    return (templatePath: string) => {
      return path.join(context.container.get(ContainerTypes.TEMPLATES_DIR), templatePath + '.html')
    }
  })
constantsContainer.bind(ContainerTypes.CACHE_DIR).toDynamicValue((context) => {
  const environment = context.container.get<IEnvironment>(ContainerTypes.ENVIRONMENT)
  const cachePath = path.join(__dirname, '../..', '.cache', environment.ACCOUNT_LOGIN)

  if (!fs.existsSync(cachePath)) {
    fs.mkdirSync(cachePath, { recursive: true })
  }

  return cachePath
})

constantsContainer.bind<string>(ContainerTypes.BROWSER_DATA_DIR).toDynamicValue((context) => {
  const environment = context.container.get<IEnvironment>(ContainerTypes.ENVIRONMENT)
  const userDataDir = path.join(context.container.get(ContainerTypes.CACHE_DIR), 'dataDir', environment.RUN_MODE)

  if (!fs.existsSync(userDataDir)) {
    fs.mkdirSync(userDataDir, { recursive: true })
  }

  // Ignore exit error from last browser run
  const preferencesPath = path.join(userDataDir, 'Default', 'Preferences')
  if (fs.existsSync(preferencesPath)) {
    const preferences = JSON.parse(fs.readFileSync(preferencesPath).toString())
    if (preferences.profile.exit_type) {
      preferences.profile.exit_type = 'Normal'
    }

    if (preferences.profile.exited_cleanly) {
      preferences.profile.exited_cleanly = true
    }

    fs.writeFileSync(preferencesPath, JSON.stringify(preferences))
  }

  return userDataDir
})

constantsContainer.bind(ContainerTypes.LOG_DIRECTORY).toDynamicValue((context) => {
  const directoryPath = path.join(
    context.container.get(ContainerTypes.CACHE_DIR),
    `log_${context.container.get(ContainerTypes.STARTUP_TIME)}`,
  )
  if (!fs.existsSync(directoryPath)) {
    fs.mkdirSync(directoryPath, { recursive: true })
  }
  return directoryPath
})

export { constantsContainer }
