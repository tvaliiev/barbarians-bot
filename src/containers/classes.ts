import { Container } from 'inversify'
import { FighterBot } from '../core/bots/FighterBot'
import { ProfileInfoUpdaterBot } from '../core/bots/ProfileInfoUpdaterBot'
import { StatsServerBot } from '../core/bots/StatsServerBot'
import { TestBot } from '../core/bots/TestBot'
import { TraderBot } from '../core/bots/TraderBot'
import { ContainerTypes } from '../helpers/ContainerValues'
import { IEnvironment, RunModes } from '../helpers/environment'
import { libsContainer } from './libs'

const container = Container.merge(libsContainer, new Container({ autoBindInjectable: true }))

const environment = container.get<IEnvironment>(ContainerTypes.ENVIRONMENT)

const bindConfig: { [key in RunModes]: (null | (new (...args: never[]) => void))[] } = {
  [RunModes.ALL]: [ProfileInfoUpdaterBot, /*TraderBot,*/ FighterBot, StatsServerBot],
  [RunModes.SIMPLE]: [ProfileInfoUpdaterBot, FighterBot, StatsServerBot],
  [RunModes.TEST]: [TestBot, StatsServerBot],
  // [RunModes.TRADER]: [TraderBot, StatsServerBot],
}

for (const bot of bindConfig[environment.RUN_MODE]) {
  if (!bot) {
    continue
  }

  container.bind(ContainerTypes.BOTS).to(bot)
}

export const classesContainer = container
