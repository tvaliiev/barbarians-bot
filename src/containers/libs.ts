import { InfluxDB } from '@influxdata/influxdb-client'
import fs from 'fs'
import { Container, interfaces } from 'inversify'
import path from 'path'
import pino from 'pino'
import pretty from 'pino-pretty'
import puppeteer, { Browser, Page } from 'puppeteer'
import { PageFactory } from '../core/PageInteraction/PageFactory'
import { PageParser } from '../core/PageInteraction/PageParser'
import { Bucket } from '../helpers/constants/Bucket'
import { ContainerTypes } from '../helpers/ContainerValues'
import { IEnvironment, LogConfig } from '../helpers/environment'
import { constantsContainer } from './constants'
import Context = interfaces.Context
import Factory = interfaces.Factory
import SimpleFactory = interfaces.SimpleFactory
import Logger = pino.Logger

const libsContainer = Container.merge(constantsContainer, new Container())

libsContainer
  .bind(ContainerTypes.BROWSER)
  .toDynamicValue(async (context) => {
    const environment = context.container.get<IEnvironment>(ContainerTypes.ENVIRONMENT)
    const browserDataDir = context.container.get(ContainerTypes.BROWSER_DATA_DIR) as string
    const browser = await puppeteer.launch({
      executablePath: environment.BROWSER_PATH || undefined,
      headless: environment.HEADLESS ? 'new' : false,
      args: [
        '--disable-background-timer-throttling',
        '--disable-backgrounding-occluded-windows',
        '--disable-renderer-backgrounding',
        '--no-sandbox',
      ],
      userDataDir: browserDataDir,
      timeout: 60_000,
    })
    const page = await browser.newPage()
    const cookiesPath = browserDataDir + '/cookies.json'
    if (fs.existsSync(cookiesPath)) {
      const content = JSON.parse(fs.readFileSync(cookiesPath).toString())
      await page.setCookie(...content)
    }
    if (!(await PageParser.isLoggedIn(page))) {
      await PageParser.login(page)
    }
    fs.writeFileSync(cookiesPath, JSON.stringify(await page.cookies()))
    await page.close()

    return browser
  })
  .inSingletonScope()

libsContainer.bind(ContainerTypes.ALL_PAGES).toConstantValue([])

libsContainer.bind(ContainerTypes.PAGE).toDynamicValue(async (context: Context) => {
  const browser = await context.container.getAsync<Browser>(ContainerTypes.BROWSER)

  const page = await PageFactory.make(browser)
  context.container.get<Page[]>(ContainerTypes.ALL_PAGES).push(page)

  return page
})

libsContainer.bind<Logger>(ContainerTypes.LOGGER_FACTORY).toFactory<unknown, [LogConfig]>((context: Context) => {
  return (config: LogConfig) => {
    const pinoStreamsConfig: { [key in LogConfig]: string[] } = {
      [LogConfig.BOTH]: ['pretty', 'file'],
      [LogConfig.CONSOLE]: ['pretty'],
    }
    const pinoStreamPresets: {
      [key: string]: () => {
        stream:
          | pino.DestinationStream
          | pino.StreamEntry<pino.Level>
          | (pino.DestinationStream | pino.StreamEntry<pino.Level>)
      }
    } = {
      pretty: () => ({
        stream: pretty({
          customPrettifiers: {
            time: (timestamp: string | object) => {
              const time = timestamp.toString().split('.')[0]
              const now = new Date()
              const date = new Date(`${now.getMonth() + 1}/${now.getDate()}/${now.getFullYear()} ${time}`)
              return '[' + date.toLocaleDateString() + ' ' + date.toLocaleTimeString() + ']'
            },
          },
          ignore: 'hostname,pid',
          sync: true,
        }),
      }),
      file: () => ({
        stream: fs.createWriteStream(path.join(context.container.get(ContainerTypes.LOG_DIRECTORY), 'default.log')),
      }),
    }

    const pinoStreams: any[] = []
    pinoStreamsConfig[config].forEach((log) => {
      pinoStreams.push(pinoStreamPresets[log]())
    })

    const pinoOptions = {
      nestedKey: 'payload',
    }

    return pino(pinoOptions, pino.multistream(pinoStreams))
  }
})

libsContainer
  .bind<Logger>(ContainerTypes.LOGGER)
  .toDynamicValue((context: Context) => {
    const factory = context.container.get<SimpleFactory<Logger>>(ContainerTypes.LOGGER_FACTORY)
    const environment = context.container.get<IEnvironment>(ContainerTypes.ENVIRONMENT)

    return factory(environment.LOG_CONFIG)
  })
  .inSingletonScope()

export type RenderFunctionArgs = {
  [key: string]: object | any[] | string | number
}
export type RenderFunction = (args: RenderFunctionArgs) => string
libsContainer
  .bind<Factory<RenderFunction>>(ContainerTypes.TEMPLATE_FACTORY)
  .toFactory<RenderFunction, [string]>((ctx) => {
    return (templatePath: string) => {
      const templatePathFactory: Factory<string> = ctx.container.get(ContainerTypes.TEMPLATE_PATH_FACTORY)

      const fullPathToTemplate: string = templatePathFactory(templatePath) as string
      if (!fs.existsSync(fullPathToTemplate)) {
        throw new Error(`No such template ${fullPathToTemplate}`)
      }

      return (args: RenderFunctionArgs = {}) => {
        let templateStr = fs.readFileSync(fullPathToTemplate).toString()

        Object.keys(args).forEach((key) => {
          const argValueType = typeof args[key]
          if (argValueType === 'object' || Array.isArray(args[key])) {
            templateStr = templateStr.replaceAll(key, Buffer.from(JSON.stringify(args[key])).toString('base64'))
            return
          }

          templateStr = templateStr.replaceAll(key, args[key] as string)
        })

        return templateStr
      }
    }
  })

libsContainer
  .bind(ContainerTypes.INFLUXDB)
  .toDynamicValue((ctx) => {
    const environment = ctx.container.get<IEnvironment>(ContainerTypes.ENVIRONMENT)
    return new InfluxDB({ url: environment.INFLUXDB_URL, token: environment.INFLUXDB_TOKEN })
  })
  .inSingletonScope()

libsContainer.bind(ContainerTypes.INFLUXDB_CLIENT).toFactory<unknown, [Bucket]>((ctx) => {
  const environment = ctx.container.get<IEnvironment>(ContainerTypes.ENVIRONMENT)
  return (bucket: Bucket) => {
    return ctx.container.get<InfluxDB>(ContainerTypes.INFLUXDB).getWriteApi(environment.INFLUXDB_ORG, bucket, 'ns')
  }
})

export { libsContainer }
