import { HandlerContract } from '../handlers/HandlerContract'

export type NextPipeline<T> = (carry: T) => Promise<T>
export type CallbackHandler<T> = (carry: T, next: NextPipeline<T>) => Promise<T>
export type HandlerTypes<T> = HandlerContract<T> | CallbackHandler<T>

export class Pipeline<Carry = any> {
  private passable: Carry | null = null
  private handlers: HandlerTypes<Carry>[] = []

  public send(passable: Carry): Pipeline<Carry> {
    this.passable = passable

    return this
  }

  public through(handlers: HandlerTypes<Carry>[]): Pipeline<Carry> {
    this.handlers = [...handlers]

    return this
  }

  public thenReturn(): Promise<Carry> {
    return this.thenExecute((passable: Carry) => passable)
  }

  public thenExecute(destination: (passable: Carry) => Carry): Promise<Carry> {
    if (this.passable === null) {
      throw new Error('Did not received passable object')
    }

    // @ts-ignore
    const pipeline: (carry: Carry) => Promise<Carry> = this.handlers
      .reverse()
      .reduce(this.carry(), this.prepareDestination(destination))

    return pipeline(this.passable)
  }

  public carry() {
    return (stack: (carry: Carry) => Promise<Carry>, pipe: HandlerTypes<Carry>) =>
      (passable: Carry): Promise<Carry> => {
        if (typeof pipe === 'function') {
          return pipe(passable, stack)
        }

        return pipe.handle(passable, stack)
      }
  }

  protected prepareDestination(destination: (passable: Carry) => Carry) {
    return (passable: Carry) => destination(passable)
  }
}
