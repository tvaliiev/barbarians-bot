import { config } from 'dotenv'
import { ObjectValidator, ObjectValidatorContract, ObjectValidatorType } from './ObjectValidator'

export enum RunModes {
  SIMPLE = 'simple',
  ALL = 'all',
  TEST = 'test',
  // TRADER = 'trader', // deprecated :(
}

export enum AccountProvider {
  SPACES = 'spaces',
  PLAIN = 'plain',
}

export enum LogConfig {
  BOTH = 'both',
  CONSOLE = 'console',
}

export enum LogLevel {
  FATAL = 'fatal',
  ERROR = 'error',
  WARN = 'warn',
  INFO = 'info',
  DEBUG = 'debug',
  TRACE = 'trace',
}

export interface IEnvironment {
  ACCOUNT_PASSWORD: string
  ACCOUNT_LOGIN: string
  HEADLESS: boolean | 'new'
  RUN_MODE: RunModes
  ACCOUNT_PROVIDER: AccountProvider
  BROWSER_PATH: string
  LOG_CONFIG: LogConfig
  INFLUXDB_TOKEN: string
  INFLUXDB_URL: string
  INFLUXDB_ORG: string
  LOG_LEVEL: LogLevel
}

const environmentValidators: ObjectValidatorContract<IEnvironment> = {
  ACCOUNT_PASSWORD: {
    type: ObjectValidatorType.STRING,
    required: true,
  },
  ACCOUNT_LOGIN: {
    type: ObjectValidatorType.STRING,
    required: true,
  },
  ACCOUNT_PROVIDER: {
    type: ObjectValidatorType.ENUM,
    enumValues: Object.values(AccountProvider),
    required: true,
  },
  RUN_MODE: {
    type: ObjectValidatorType.ENUM,
    enumValues: Object.values(RunModes),
    default: RunModes.ALL,
  },
  HEADLESS: {
    type: ObjectValidatorType.BOOLEAN,
    default: false,
  },
  BROWSER_PATH: {
    type: ObjectValidatorType.STRING,
    default: '',
  },
  LOG_CONFIG: {
    type: ObjectValidatorType.ENUM,
    enumValues: Object.values(LogConfig),
    default: LogConfig.BOTH,
  },
  LOG_LEVEL: {
    type: ObjectValidatorType.ENUM,
    enumValues: Object.values(LogLevel),
    default: LogLevel.INFO,
  },
  INFLUXDB_TOKEN: {
    type: ObjectValidatorType.STRING,
    required: true,
  },
  INFLUXDB_URL: {
    type: ObjectValidatorType.STRING,
    required: true,
  },
  INFLUXDB_ORG: {
    type: ObjectValidatorType.STRING,
    required: true,
  },
}

const result = config()
// @ts-ignore
if (result.error && result.error.errno !== -2) {
  throw result.error
}

const validationResult = ObjectValidator.validate<IEnvironment>(process.env, environmentValidators)
if (!validationResult.validated) {
  throw new Error(validationResult.errors.join(`\n`))
}

export const environment = validationResult.validated
