export class FixedSizeArray<T> {
  constructor(
    private maxSize: number,
    private items: T[] = [],
  ) {}

  public push(...items: T[]) {
    this.items.push(...items)
    if (this.items.length > this.maxSize) {
      this.items.splice(0, this.items.length - this.maxSize)
    }
  }

  public getItems(): T[] {
    return this.items
  }

  public getLast(): T | undefined {
    return this.items[this.items.length - 1]
  }
}
