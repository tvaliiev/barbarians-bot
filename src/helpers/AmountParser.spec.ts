import { AmountParser } from './AmountParser'

describe('AmountParser.REGEX', () => {
  it.each([['123'], ['1.23K'], ['1K'], ['1M'], ['1.1M'], ['1.0M']])('should match various formats', (str) => {
    expect(str.match(AmountParser.REGEX))
  })
})

describe('AmountParser.toNumber', () => {
  it.each([
    ['123', 123],
    ['123K', 123_000],
    ['1.23K', 1_230],
    ['1K', 1_000],
    ['1M', 1_000_000],
    ['1.1M', 1_100_000],
    ['1.0M', 1_000_000],
  ])('should parse correctly', (str, expected) => {
    expect(AmountParser.toNumber(str)).toEqual(expected)
  })
})
