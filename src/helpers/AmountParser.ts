export class AmountParser {
  public static readonly REGEX = '\\d+\\.?\\d?(?:K|M)?'

  public static toNumber(value: string): number {
    let result = parseFloat(value)
    const suffixes: { [key: string]: number } = {
      M: 1_000_000,
      K: 1_000,
    }

    if (suffixes[value[value.length - 1]]) {
      result *= suffixes[value[value.length - 1]]
    }

    return result
  }
}
