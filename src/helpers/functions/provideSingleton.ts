import { fluentProvide } from 'inversify-binding-decorators'

export function provideSingleton(args: any) {
  return fluentProvide(args).inSingletonScope().done()
}
