import delay from './delay'

export async function waitFor(check: () => Promise<boolean>, repeatInterval: number = 2000): Promise<void> {
  while (true) {
    if (await check()) {
      break
    }

    await delay(repeatInterval)
  }
}
