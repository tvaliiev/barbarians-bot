export function toRelativeTime(time: number) {
  return Math.round((time - Date.now()) / 1000)
}
