import { parseTimeInSimpleFormat } from './ParseTimeInSimpleFormat'

describe('parseTimeInSimpleFormat', () => {
  it.each([
    ['00:00:05', 5],
    ['00:05:05', 305],
    ['02:10:05', 7805],
    ['1:55:00', 60 * 60 + 55 * 60],
  ])('should correctly parse time "%s" into %d', (str, expectedTime) => {
    expect(parseTimeInSimpleFormat(str)).toEqual(expectedTime)
  })
})
