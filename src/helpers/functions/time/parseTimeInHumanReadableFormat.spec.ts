import { parseTimeInHumanReadableFormat } from './parseTimeInHumanReadableFormat'

describe('parseTimeInHumanReadableFormat', () => {
  it.each([
    ['hours+minutes+seconds', '2ч. 37мин. 49сек.', 2 * 60 * 60 + 37 * 60 + 49],
    ['hours+seconds', '2ч. 59сек.', 2 * 60 * 60 + 59],
    ['hours+seconds with disruptions', '15ч. 54мин.\n\n', 15 * 60 * 60 + 54 * 60],
    ['hours+minutes', '1ч. 37мин.', 60 * 60 + 37 * 60],
    ['minutes+seconds', '22мин. 9сек.', 22 * 60 + 9],
    ['seconds', '59сек.', 59],
    ['minutes', '55мин.', 55 * 60],
    ['hours', '12ч.', 12 * 60 * 60],
    ['hours+minutes+seconds in other format', '2h 37m 49s', 2 * 60 * 60 + 37 * 60 + 49],
    ['hours+seconds in other format', '2h 59s', 2 * 60 * 60 + 59],
    ['hours+minutes in other format', '1h 37m', 60 * 60 + 37 * 60],
    ['minutes+seconds in other format', '22m 9s', 22 * 60 + 9],
    ['seconds in other format', '59s', 59],
    ['minutes in other format', '55m', 55 * 60],
    ['hours in other format', '12h', 12 * 60 * 60],
    ['days+seconds', '1д. 25сек.', 24 * 60 * 60 + 25],
  ])('should parse %s', (_, input, expected) => {
    expect(parseTimeInHumanReadableFormat(input)).toEqual(expected)
  })
})
