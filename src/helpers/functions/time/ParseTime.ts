import { parseTimeInHumanReadableFormat } from './parseTimeInHumanReadableFormat'
import { parseTimeInSimpleFormat } from './ParseTimeInSimpleFormat'

export enum TimeFormat {
  HUMAN_READABLE = 'human-readable',
  SIMPLE = 'simple',
}

export enum TimeOutputFormat {
  RELATIVE_MILLISECONDS = 'relative-milliseconds',
  AS_IS_SECONDS = 'as-is-seconds',
  AS_IS_MILLISECONDS = 'as-is-milliseconds',
}

const timeParsers: { [key in TimeFormat]: (time: string) => number } = {
  [TimeFormat.SIMPLE]: parseTimeInSimpleFormat,
  [TimeFormat.HUMAN_READABLE]: parseTimeInHumanReadableFormat,
}

export function parseTime(
  time: string,
  inputFormat: TimeFormat = TimeFormat.HUMAN_READABLE,
  outputFormat: TimeOutputFormat = TimeOutputFormat.AS_IS_MILLISECONDS,
): number {
  let result = timeParsers[inputFormat](time)

  if (outputFormat === TimeOutputFormat.RELATIVE_MILLISECONDS || outputFormat === TimeOutputFormat.AS_IS_MILLISECONDS) {
    result *= 1000
  }

  if (TimeOutputFormat.RELATIVE_MILLISECONDS === outputFormat) {
    result += Date.now()
  }

  return result
}
