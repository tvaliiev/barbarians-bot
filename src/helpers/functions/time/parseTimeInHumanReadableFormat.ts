import { parseTimeInSimpleFormat } from './ParseTimeInSimpleFormat'

const timeParts = [
  ['д.', 'd'],
  ['ч.', 'h'],
  ['мин.', 'm'],
  ['сек.', 's'],
]
export const RELATIVE_TIME_REGEX = timeParts
  .map((possibleParts) => {
    const possiblePartsStr = possibleParts
      .map((part) => {
        return '(?:' + part.replaceAll('.', '\\.') + ')'
      })
      .join('|')

    return `((\\d+)(${possiblePartsStr})\\s*)?`
  })
  .join('')

export function parseTimeInHumanReadableFormat(time: string): number {
  const parsedTime = ['0', '00', '00', '00']
  const matches = Array.from(time.matchAll(new RegExp(RELATIVE_TIME_REGEX, 'g')))
  const match = matches[0]
  for (let i = 1; i < match.length; i += 3) {
    if (!match[i]) {
      continue
    }
    const timeUnit = match[i + 2].trim()
    const number = match[i + 1].trim()

    const partNumber = timeParts.findIndex((parts) => {
      return parts.find((part) => {
        return part === timeUnit
      })
    })
    if (partNumber === -1) {
      throw new Error(`Cant parse ${timeUnit}`)
    }

    parsedTime[partNumber] = number
  }

  return parseTimeInSimpleFormat(parsedTime.join(':'))
}
