export const SIMPLE_TIME_REGEXP = '(?:\\d{2}:){0,2}(?:\\d{2})'

const timeMultipliers = [
  24 * 60 * 60, //days
  60 * 60, //hours
  60, //minutes
  1, //seconds
].reverse()

export function parseTimeInSimpleFormat(time: string): number {
  return time
    .split(':')
    .reverse()
    .reduce((acc, current, i) => acc + parseInt(current) * timeMultipliers[i], 0)
}
