export function objectGet<T = unknown>(
  target: { [key: string | number | symbol]: any },
  fieldPath: string | string[],
  defaultValue: T | undefined = undefined,
): T | undefined {
  if (typeof fieldPath === 'string') {
    fieldPath = fieldPath.split('.')
  }

  const currentField = fieldPath.shift()

  if (!currentField) {
    throw new Error()
  }

  if (fieldPath.length === 0) {
    return Object.hasOwn(target, currentField) ? target[currentField] : defaultValue
  }

  if (typeof target[currentField] !== 'object') {
    return defaultValue
  }

  return objectGet(target[currentField], fieldPath, defaultValue)
}

export function objectSet<T = unknown>(
  target: { [key: string | number | symbol]: any },
  fieldPath: string | string[],
  value: T | undefined = undefined,
  options: { create?: boolean } = {},
): boolean {
  if (typeof fieldPath === 'string') {
    fieldPath = fieldPath.split('.')
  }

  const currentField = fieldPath.shift()

  if (!currentField) {
    throw new Error()
  }

  if (fieldPath.length === 0) {
    target[currentField] = value
    return true
  }

  if (typeof target[currentField] !== 'object') {
    if (options.create) {
      target[currentField] = {}
    } else {
      return false
    }
  }

  return objectSet(target[currentField], fieldPath, value, options)
}
