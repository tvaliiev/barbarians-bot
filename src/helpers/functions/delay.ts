export default async function delay(ms: number) {
  if (ms <= 0) {
    return
  }

  return new Promise<void>((resolve) => {
    setTimeout(() => {
      resolve()
    }, ms)
  })
}
