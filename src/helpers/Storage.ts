import * as fs from 'fs'
import { inject } from 'inversify'
import path from 'path'
import _ from 'underscore'
import { UserBodyPageActionsData } from '../core/PageInteraction/PageActions/UserBodyPageActions'
import { ContainerTypes } from './ContainerValues'
import { objectGet, objectSet } from './functions/ObjectGetterAndSetter'
import { provideSingleton } from './functions/provideSingleton'
import { parseTime } from './functions/time/ParseTime'
import { LiveStatsContract } from './LiveStats'

interface StorageContract {
  lastActionTime: number
  globalDelay: number
  actionDelay: number
  dungeonAvailableAt: number
  arenaAvailableAt: number
  survivalAvailableAt: number
  duelsAvailableAt: number
  ogreDungeonAvailableAt: number
  battlefieldsAvailableAt: number
  hero: { [key: string]: any }
  accountHash: string
  liveStats: { [key: string]: any } | null
  userBodyPageActionsData?: UserBodyPageActionsData

  _dates: { [key: string]: any }
}

@provideSingleton(Storage)
export class Storage {
  private readonly storagePath
  private storage: StorageContract = {
    lastActionTime: 0,
    globalDelay: 1000,
    actionDelay: 3500,
    dungeonAvailableAt: 0,
    arenaAvailableAt: 0,
    survivalAvailableAt: 0,
    duelsAvailableAt: 0,
    ogreDungeonAvailableAt: 0,
    battlefieldsAvailableAt: 0,
    hero: {},
    accountHash: '',
    liveStats: null,
    _dates: {},
  }

  constructor(
    @inject(ContainerTypes.CACHE_DIR)
    cacheDir: string,
  ) {
    this.storagePath = path.join(cacheDir, 'storage.json')
    this.readFromPermanentStorage()

    this.writeToPermanentStorage = _.throttle(this.writeToPermanentStorage, 10000)
  }

  public get userBodyPageActionsData(): any {
    return this.storage.userBodyPageActionsData
  }

  public set userBodyPageActionsData(value: UserBodyPageActionsData) {
    this.storage.userBodyPageActionsData = value
    this.updateTimeInHumanReadableFormat('userBodyPageActionsData.latestCantRepairTime')
    this.writeToPermanentStorage()
  }

  public get battlefieldsAvailableAt(): number {
    return this.storage.battlefieldsAvailableAt
  }

  public set battlefieldsAvailableAt(value: number) {
    this.storage.battlefieldsAvailableAt = value
    this.updateTimeInHumanReadableFormat('battlefieldsAvailableAt')
    this.writeToPermanentStorage()
  }

  public get liveStats(): any {
    return this.storage.liveStats
  }

  public set liveStats(value: LiveStatsContract) {
    this.storage.liveStats = value
    this.writeToPermanentStorage()
  }

  public get accountHash(): string {
    return this.storage.accountHash
  }

  public set accountHash(value: string) {
    this.storage.accountHash = value
    this.writeToPermanentStorage()
  }

  public get ogreDungeonAvailableAt(): number {
    return this.storage.ogreDungeonAvailableAt
  }

  public set ogreDungeonAvailableAt(value: number) {
    this.storage.ogreDungeonAvailableAt = value
    this.updateTimeInHumanReadableFormat('ogreDungeonAvailableAt')
    this.writeToPermanentStorage()
  }

  public get duelsAvailableAt(): number {
    return this.storage.duelsAvailableAt
  }

  public set duelsAvailableAt(value: number) {
    this.storage.duelsAvailableAt = value
    this.updateTimeInHumanReadableFormat('duelsAvailableAt')
    this.writeToPermanentStorage()
  }

  public get survivalAvailableAt(): number {
    return this.storage.survivalAvailableAt
  }

  public set survivalAvailableAt(value: number) {
    this.storage.survivalAvailableAt = value
    this.updateTimeInHumanReadableFormat('survivalAvailableAt')
    this.writeToPermanentStorage()
  }

  public get arenaAvailableAt(): number {
    return this.storage.arenaAvailableAt
  }

  public set arenaAvailableAt(value: number) {
    this.storage.arenaAvailableAt = value
    this.updateTimeInHumanReadableFormat('arenaAvailableAt')
    this.writeToPermanentStorage()
  }

  public get dungeonAvailableAt(): number {
    return this.storage.dungeonAvailableAt
  }

  public set dungeonAvailableAt(value: number) {
    this.storage.dungeonAvailableAt = value
    this.updateTimeInHumanReadableFormat('dungeonAvailableAt')
    this.writeToPermanentStorage()
  }

  public get actionDelay(): number {
    return this.storage.actionDelay
  }

  public set actionDelay(value: number) {
    this.storage.actionDelay = Math.max(Math.min(value, 5000), 1500)
    this.writeToPermanentStorage()
  }

  public get lastActionTime(): number {
    return this.storage.lastActionTime
  }

  public set lastActionTime(value: number) {
    this.storage.lastActionTime = value
    this.writeToPermanentStorage()
  }

  public get globalDelay(): number {
    return this.storage.globalDelay
  }

  public set globalDelay(value: number) {
    this.storage.globalDelay = value
    this.writeToPermanentStorage()
  }

  public set hero(value: { [key: string]: any }) {
    this.storage.hero = value
    this.updateTimeInHumanReadableFormat('hero.altarBuff.availableTill')
    this.writeToPermanentStorage()
  }

  public get actionDelayWithGlobal(): number {
    return this.actionDelay + this.globalDelay
  }

  public get actionDelayWithGlobalInPvP(): number {
    return this.actionDelayWithGlobal * 0.9
  }

  public get actionDelayInPVP(): number {
    return this.actionDelay * 0.9
  }

  public postponeDungeonFight(time: number = parseTime('4m')) {
    this.dungeonAvailableAt = Date.now() + time
  }

  public postponeSurvivalFight(time: number = parseTime('3m')) {
    this.survivalAvailableAt = Date.now() + time
  }

  public postponeArenaFight(time: number = parseTime('3m')) {
    this.arenaAvailableAt = Date.now() + time
  }

  public reset() {
    this.readFromPermanentStorage()
  }

  protected writeToPermanentStorage() {
    fs.writeFileSync(this.storagePath, JSON.stringify(this.storage, undefined, 2), { flag: 'w+' })
  }

  protected readFromPermanentStorage() {
    if (!fs.existsSync(this.storagePath)) {
      return
    }

    this.storage = Object.assign(this.storage, JSON.parse(fs.readFileSync(this.storagePath, { encoding: 'utf8' })))
  }

  protected updateTimeInHumanReadableFormat(field: string) {
    const value = objectGet(this.storage, field)
    if (typeof value !== 'number') {
      return
    }

    if (!field.endsWith('AvailableAt') && !field.endsWith('Till')) {
      return
    }

    const date = new Date(value)
    if (!objectSet(this.storage._dates, field, date.toLocaleString(), { create: true })) {
      throw new Error('Cannot set field')
    }
  }
}
