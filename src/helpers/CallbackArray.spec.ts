import { CallbackArray, CallbackMode } from './CallbackArray'
import { CallbackHandler, NextPipeline } from './Pipeline'

describe('CallbackArray', () => {
  it('should properly break in the middle', async () => {
    type CBType = number
    const cbArray = new CallbackArray<CBType>()
    const executionStack: string[] = []

    const object: { [key: string]: CallbackHandler<CBType> } = {
      cb1(current: CBType, next: NextPipeline<CBType>) {
        executionStack.push('cb1')
        return next(current)
      },
      cb2(current: CBType, next: NextPipeline<CBType>) {
        executionStack.push('cb2')
        return next(current)
      },
      async cb3(current: CBType) {
        executionStack.push('cb3')
        return current + 2
      },
      cb4(current: CBType, next: NextPipeline<CBType>) {
        executionStack.push('cb1')
        return next(current)
      },
    }

    Object.keys(object).forEach((key) => {
      for (let i = 0; i < 5; i++) {
        cbArray.push(key, object[key])
      }
    })

    expect(Object.keys(cbArray['callbacks']).length).toEqual(4)

    const result = await cbArray.execute(10)
    expect(result).toEqual(12)
    expect(executionStack).toMatchObject(['cb1', 'cb2', 'cb3'])
  })

  it('should return result without any errors', async () => {
    type CBType = number
    const cbArray = new CallbackArray<CBType>()

    const object: { [key: string]: CallbackHandler<CBType> } = {
      cb1(current: CBType, next: NextPipeline<CBType>) {
        return next(current)
      },
      cb2(current: CBType, next: NextPipeline<CBType>) {
        return next(current)
      },
      cb4(current: CBType, next: NextPipeline<CBType>) {
        return next(current)
      },
    }

    Object.keys(object).forEach((key) => {
      for (let i = 0; i < 5; i++) {
        cbArray.push(key, object[key])
      }
    })

    expect(Object.keys(cbArray['callbacks']).length).toEqual(3)

    const result = await cbArray.execute(10)
    expect(result).toEqual(10)
  })

  it('should not replace existing function when key exists', async () => {
    type CBType = number
    const cbArray = new CallbackArray<CBType>()

    const object: { [key: string]: CallbackHandler<CBType> } = {
      cb1(current: CBType, next: NextPipeline<CBType>) {
        return next(current)
      },
      cb2(current: CBType, next: NextPipeline<CBType>) {
        return next(current)
      },
    }

    Object.keys(object).forEach((key) => {
      cbArray.push(key, object[key])
    })

    const replaceFn = jest.fn()
    cbArray.push('cb1', replaceFn)

    const result = await cbArray.execute(10)
    expect(result).toEqual(10)
    expect(replaceFn).not.toHaveBeenCalled()
  })

  it('should forget single call callback after executing', async () => {
    type CBType = number
    const cbArray = new CallbackArray<CBType>()

    const object: {
      [key: string]: { callback: CallbackHandler<CBType>; mode: CallbackMode }
    } = {
      cb1: {
        callback(current: CBType, next: NextPipeline<CBType>) {
          return next(current)
        },
        mode: CallbackMode.ONCE,
      },
      cb2: {
        callback(current: CBType, next: NextPipeline<CBType>) {
          return next(current)
        },
        mode: CallbackMode.ONCE,
      },
      cb5: {
        callback(current: CBType, next: NextPipeline<CBType>) {
          return next(current)
        },
        mode: CallbackMode.FOREVER,
      },
      cb3: {
        callback(current: CBType, next: NextPipeline<CBType>) {
          return next(current)
        },
        mode: CallbackMode.ONCE,
      },
      cb4: {
        callback(current: CBType, next: NextPipeline<CBType>) {
          return next(current)
        },
        mode: CallbackMode.ONCE,
      },
    }

    Object.keys(object).forEach((key) => {
      object[key].callback = jest.fn(object[key].callback)
      cbArray.push(key, object[key].callback, object[key].mode)
    })

    const result = await cbArray.execute(10)
    expect(result).toEqual(10)

    Object.keys(object).forEach((key) => {
      expect(object[key].callback).toHaveBeenCalled()
    })

    expect(Object.keys(cbArray['callbacks']).length).toEqual(1)
  })
})
