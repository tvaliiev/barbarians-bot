export enum ObjectValidatorType {
  STRING = 'string',
  BOOLEAN = 'boolean',
  ENUM = 'enum',
  NUMBER = 'number',
}

export type ObjectValidatorContract<T> = {
  [key in keyof T]: {
    type: ObjectValidatorType
    enumValues?: (string | number | boolean)[]
    required?: boolean
    default?: any
  }
}
export type ObjectValidatorTarget = {
  [key: string]: any
}
export type ObjectValidatorResponse<T extends ObjectValidatorTarget> = {
  errors: string[]
  validated: T | null
}

export class ObjectValidator {
  public static validate<T extends ObjectValidatorTarget>(
    data: ObjectValidatorTarget,
    rules: ObjectValidatorContract<T>,
  ): ObjectValidatorResponse<T> {
    const errors: string[] = []

    const validated: ObjectValidatorTarget = {}

    for (let key in rules) {
      let value: any = data[key]
      const currentValidator = rules[key]

      if (typeof value === 'undefined') {
        if (currentValidator.required) {
          errors.push(`Environment variable ${key} is required`)
          continue
        }

        validated[key] = currentValidator.default
        continue
      }

      switch (currentValidator.type) {
        case ObjectValidatorType.BOOLEAN:
          if (value === 'true') {
            value = true
          } else if (value === 'false') {
            value = false
          } else {
            errors.push(`Unexpected environment variable ${key} value (${value}). Expected: true, false`)
            continue
          }
          break
        case ObjectValidatorType.ENUM:
          if (!currentValidator.enumValues) {
            errors.push(`You should specify enum values for environment key ${key}`)
            continue
          }

          if (!Object.values(currentValidator.enumValues).find((v) => v === value)) {
            const expectedValues = currentValidator.enumValues.join(', ')
            errors.push(`Unexpected enum value for environment variable "${key}". Expected: ${expectedValues}`)
            continue
          }
          break
        case ObjectValidatorType.NUMBER:
          if (typeof value !== 'number') {
            const parsedNumber = parseInt(value)
            if (Number.isNaN(parsedNumber)) {
              errors.push(`Unexpected number value for environment variable "${key}".`)
              continue
            }

            value = parsedNumber
          }
          break
        case ObjectValidatorType.STRING:
          break
      }

      validated[key] = value
    }

    return {
      validated: errors.length ? null : (validated as T),
      errors,
    }
  }
}
