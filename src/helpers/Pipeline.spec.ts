import { HandlerContract } from '../handlers/HandlerContract'
import { NextPipeline, Pipeline } from './Pipeline'

describe('Pipeline', () => {
  it('should work', async () => {
    class Test implements HandlerContract<{ test: number }> {
      public handle(carry: { test: number }, next: NextPipeline<{ test: number }>): Promise<{ test: number }> {
        expect(carry).toMatchObject({ test: 1 })
        return next(carry)
      }
    }

    const handlers = [
      jest.fn((passed: any, next: (arg0: any) => any) => {
        expect(passed).toMatchObject({ test: 1 })
        return next(passed)
      }),
      new Test(),
      jest.fn(() => true),
      jest.fn(() => false),
    ]
    // @ts-ignore
    const objectSpy = jest.spyOn(handlers[1], 'handle')

    const result = await new Pipeline()
      .send({ test: 1 })
      .through(handlers as any)
      .thenReturn()

    expect(result).toStrictEqual(true)
    expect(objectSpy).toHaveBeenCalled()
    expect(handlers[0]).toHaveBeenCalled()
    expect(handlers[2]).toHaveBeenCalled()
    expect(handlers[3]).not.toHaveBeenCalled()
  })

  it('should throw error when no passable provided', async () => {
    await expect(async () => await new Pipeline().thenReturn()).rejects.toThrow('not received passable')
  })

  it('should not launch when not used', async () => {
    const handlers = [
      jest.fn((passed: any, next: (arg0: any) => any) => {
        return next(passed)
      }),
      jest.fn((passed: any, next: NextPipeline<any>) => {
        return next(passed)
      }),
    ]

    await new Pipeline().send(1).through(handlers)

    expect(handlers[0]).not.toHaveBeenCalled()
    expect(handlers[1]).not.toHaveBeenCalled()
  })
})
