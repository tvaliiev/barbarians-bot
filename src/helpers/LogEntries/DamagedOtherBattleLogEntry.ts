import { BattleLogEntry } from './BattleLogEntry'

export class DamagedOtherBattleLogEntry extends BattleLogEntry {
  public readonly parsedData: { damageAmount: number; isCritical: boolean; damageBy: string; damaged: string }

  constructor(text: string) {
    super(text)

    this.parsedData = {
      damageAmount: this.numbers[0],
      isCritical: this.isCritical(text),
      damageBy: this.getFirstWord(text),
      damaged: this.getWord(text, 2),
    }
  }
}
