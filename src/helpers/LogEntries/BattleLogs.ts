import { Page } from 'puppeteer'
import { Hero } from '../../core/Hero'
import { PageParser } from '../../core/PageInteraction/PageParser'
import { BattleLogEntry } from './BattleLogEntry'

export class BattleLogs {
  entries: BattleLogEntry[] = []
  newEntries: BattleLogEntry[] = []

  public static async updateBattleLogFromPageToHero(page: Page, hero: Hero): Promise<boolean> {
    const logs = await PageParser.getBattleLog(page)
    if (logs === null) {
      return false
    }

    hero.battleLogs.newEntries = logs.filter(
      (possibleNewEntry) => !hero.battleLogs.entries.find((old) => possibleNewEntry.text === old.text),
    )
    hero.battleLogs.entries = logs
    return true
  }
}
