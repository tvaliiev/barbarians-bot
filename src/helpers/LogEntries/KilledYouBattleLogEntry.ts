import { BattleLogEntry } from './BattleLogEntry'

export class KilledYouBattleLogEntry extends BattleLogEntry {
  public readonly parsedData: { killedBy: string }

  constructor(text: string) {
    super(text)

    let killedBy = this.getFirstWord(text)
    this.parsedData = {
      killedBy: killedBy ? killedBy[0] : '',
    }
  }
}
