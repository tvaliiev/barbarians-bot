import { BattleLogEntry } from './BattleLogEntry'

export class DamagedYouBattleLogEntry extends BattleLogEntry {
  public readonly parsedData: { damageAmount: number; isCritical: boolean; damageBy: string }

  constructor(text: string) {
    super(text)

    this.parsedData = {
      damageAmount: this.numbers[0],
      isCritical: this.isCritical(text),
      damageBy: this.getFirstWord(text),
    }
  }
}
