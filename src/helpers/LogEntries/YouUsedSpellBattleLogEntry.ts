import { BattleLogEntry } from './BattleLogEntry'

export class YouUsedSpellBattleLogEntry extends BattleLogEntry {
  public parsedData: { spellName: string }

  constructor(text: string) {
    super(text)

    this.parsedData = {
      spellName: this.getTextAfter(text, 'включила ') || this.getTextAfter(text, 'включил '),
    }
  }
}
