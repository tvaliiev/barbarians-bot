import { OtherHealedYouBattleLogEntry } from './OtherHealedYouBattleLogEntry'
import { KilledYouBattleLogEntry } from './KilledYouBattleLogEntry'
import { DamagedYouBattleLogEntry } from './DamagedYouBattleLogEntry'
import { BattleLogEntry } from './BattleLogEntry'
import { DamagedOtherBattleLogEntry } from './DamagedOtherBattleLogEntry'
import { SomeoneUsedSpellBattleLogEntry } from './SomeoneUsedSpellBattleLogEntry'
import { DrainedEnergyFromYouBattleLogEntry } from './DrainedEnergyFromYouBattleLogEntry'
import { YouDrainedEnergyBattleLogEntry } from './YouDrainedEnergyBattleLogEntry'
import { YouUsedSpellBattleLogEntry } from './YouUsedSpellBattleLogEntry'
import { BattleLogEntryType } from './BattleLogEntryType'

export class BattleLogEntryFactory {
  public static getLogType(str: string): BattleLogEntryType {
    const config: { [key: string]: string[] } = {
      [BattleLogEntryType.OTHER_HEALED_YOU]: ['полечила тебя', 'полечил тебя'],
      [BattleLogEntryType.KILLED_YOU]: ['убил тебя', 'убила тебя'],
      [BattleLogEntryType.DAMAGED_YOU]: ['ударил тебя', 'ударила тебя'],
      [BattleLogEntryType.DAMAGED_OTHER]: ['ударил', 'ударила'],
      [BattleLogEntryType.SOMEONE_USED_SPELL]: ['применил', 'применила'],
      [BattleLogEntryType.DRAINED_ENERGY_FROM_YOU]: ['сжёг у тебя', 'сожгла у тебя'],
      [BattleLogEntryType.YOU_DRAINED_ENERGY]: ['Ты сжёг', 'Ты сожгла'],
      [BattleLogEntryType.YOU_USED_SPELL]: ['Ты включил', 'Ты включила'],
      [BattleLogEntryType.UNKNOWN]: [],
    }

    for (const configKey in config) {
      const result = (config[configKey] as string[]).find((substring) => {
        return str.indexOf(substring) >= 0
      })
      if (result) {
        return configKey as BattleLogEntryType
      }
    }

    return BattleLogEntryType.UNKNOWN
  }

  public static createFromString(str: string): BattleLogEntry | null {
    const type = BattleLogEntryFactory.getLogType(str)
    if (type == BattleLogEntryType.UNKNOWN) return null

    const config: { [key in BattleLogEntryType]: (str: string) => BattleLogEntry } = {
      [BattleLogEntryType.OTHER_HEALED_YOU]: (str) => new OtherHealedYouBattleLogEntry(str),
      [BattleLogEntryType.KILLED_YOU]: (str) => new KilledYouBattleLogEntry(str),
      [BattleLogEntryType.DAMAGED_YOU]: (str) => new DamagedYouBattleLogEntry(str),
      [BattleLogEntryType.DAMAGED_OTHER]: (str) => new DamagedOtherBattleLogEntry(str),
      [BattleLogEntryType.SOMEONE_USED_SPELL]: (str) => new SomeoneUsedSpellBattleLogEntry(str),
      [BattleLogEntryType.DRAINED_ENERGY_FROM_YOU]: (str) => new DrainedEnergyFromYouBattleLogEntry(str),
      [BattleLogEntryType.YOU_DRAINED_ENERGY]: (str) => new YouDrainedEnergyBattleLogEntry(str),
      [BattleLogEntryType.YOU_USED_SPELL]: (str) => new YouUsedSpellBattleLogEntry(str),
      [BattleLogEntryType.UNKNOWN]: (str) => new BattleLogEntry(str),
    }

    return config[type](str)
  }
}
