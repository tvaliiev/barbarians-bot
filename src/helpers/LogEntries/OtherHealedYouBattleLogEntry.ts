import { BattleLogEntry } from './BattleLogEntry'

export class OtherHealedYouBattleLogEntry extends BattleLogEntry {
  public readonly parsedData: { healAmount: number; isCritical: boolean }

  constructor(text: string) {
    super(text)

    this.parsedData = {
      healAmount: this.numbers[0],
      isCritical: this.isCritical(text),
    }
  }
}
