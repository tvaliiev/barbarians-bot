import { BattleLogEntry } from './BattleLogEntry'

export class SomeoneUsedSpellBattleLogEntry extends BattleLogEntry {
  public parsedData: { spellName: string }

  constructor(text: string) {
    super(text)

    this.parsedData = {
      spellName: this.getTextAfter(text, 'применила ') || this.getTextAfter(text, 'применил '),
    }
  }
}
