export enum BattleLogEntryType {
  OTHER_HEALED_YOU = 'other_healed_you',
  // OTHER_HEALED_OTHER = 'other_healed_other',
  KILLED_YOU = 'killed_you',
  // KILLED_OTHER = 'killed_other',
  DAMAGED_YOU = 'damaged_you',
  DAMAGED_OTHER = 'damaged_other',
  SOMEONE_USED_SPELL = 'someone_used_spell',
  DRAINED_ENERGY_FROM_YOU = 'drained_energy_from_you',
  // DRAINED_ENERGY_FROM_OTHER = 'drained_energy_from_other',
  YOU_DRAINED_ENERGY = 'you_drained_energy',
  YOU_USED_SPELL = 'you_used_spell',
  UNKNOWN = 'unknown',
}
