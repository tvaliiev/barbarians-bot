export class BattleLogEntry {
  public readonly parsedData: object = {}
  protected readonly numbers: number[]

  constructor(public readonly text: string) {
    this.numbers = this.getNumbers(text)
  }

  protected getNumbers(text: string) {
    return [...text.matchAll(new RegExp('\\d+', 'g'))].map((e) => (e ? parseInt(e[0]) : -1))
  }

  protected isCritical(str: string): boolean {
    return str.endsWith('крит')
  }

  protected getFirstWord(text: string): string {
    const match = text.match('^\\w+')
    return match ? match[0] : ''
  }

  protected getWord(text: string, index: number): string {
    const match = [...text.matchAll(new RegExp('\\w+', 'g'))]
    return match[index] ? match[index][0] : ''
  }

  protected getTextAfter(text: string, substring: string): string {
    const match = text.indexOf(substring)
    if (match == -1) {
      return ''
    }

    return text.substring(match + substring.length)
  }
}
