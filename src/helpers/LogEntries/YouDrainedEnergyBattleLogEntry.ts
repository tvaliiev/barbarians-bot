import { BattleLogEntry } from './BattleLogEntry'

export class YouDrainedEnergyBattleLogEntry extends BattleLogEntry {
  parsedData: { drainedAmount: number; drainedFrom: string }
  constructor(text: string) {
    super(text)

    this.parsedData = {
      drainedAmount: this.numbers[0],
      drainedFrom: this.getTextAfter(text, ' у '),
    }
  }
}
