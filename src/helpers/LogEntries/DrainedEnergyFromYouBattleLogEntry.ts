import { BattleLogEntry } from './BattleLogEntry'

export class DrainedEnergyFromYouBattleLogEntry extends BattleLogEntry {
  public parsedData: { drainedBy: string; drainedAmount: number }
  constructor(text: string) {
    super(text)

    this.parsedData = {
      drainedBy: this.getFirstWord(text),
      drainedAmount: this.numbers[0],
    }
  }
}
