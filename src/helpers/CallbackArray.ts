import { CallbackHandler, Pipeline } from './Pipeline'

export enum CallbackMode {
  ONCE,
  FOREVER,
}

export class CallbackArray<T> {
  private callbacks: {
    [key: string]: {
      callback: CallbackHandler<T>
      mode: CallbackMode
    }
  } = {}

  public push(key: string, callback: CallbackHandler<T>, mode: CallbackMode = CallbackMode.FOREVER): void {
    if (this.callbacks[key]) {
      return
    }

    this.callbacks[key] = { callback, mode }
  }

  public execute(passable: T) {
    return new Pipeline<T>()
      .send(passable)
      .through(Object.values(this.callbacks).map((o) => o.callback))
      .thenExecute((result) => {
        this.getRidOfSingleTimeUseCallbacks()

        return result
      })
  }

  protected getRidOfSingleTimeUseCallbacks() {
    Object.keys(this.callbacks).forEach((key) => {
      if (this.callbacks[key].mode === CallbackMode.ONCE) {
        delete this.callbacks[key]
      }
    })
  }
}
