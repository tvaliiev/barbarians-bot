import { TalentName, TalentPriorities } from '../core/PageInteraction/PageParsers/TalentPageParser'

export class HeroConfig {
  public static async getTalentUpgradePriorities(): Promise<TalentPriorities> {
    const map: TalentPriorities = new Map()

    map.set(TalentName.BURNING_ANGER, 10)
    map.set(TalentName.HARDENING, 10)

    const atlasTree = new Map()
    atlasTree.set(TalentName.SEARING_ENERGY_SHIELD, 2)
    atlasTree.set(TalentName.ACCELERATED_RYTHM, 3)
    atlasTree.set(TalentName.CRIT_RELEASE, 3)
    atlasTree.set(TalentName.EVASION, 2)
    atlasTree.set(TalentName.ATLAS_AGILITY, 1)
    map.set(TalentName.ATLAS_TREE, atlasTree)

    map.set(TalentName.RELIABLE_ARMOUR, 10)
    map.set(TalentName.FAME, 10)
    map.set(TalentName.HEALING_POTIONS, 10)

    return map
  }
}
