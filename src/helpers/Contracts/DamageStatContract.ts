export enum DamageType {
  HEAL = 'heal',
  ATTACK = 'attack',
  BURN_ENERGY = 'burn-energy',
  MISS = 'miss',
}

export interface DamageStatContract {
  damage: number
  delay: number
  isCrit: boolean
  type: DamageType
}
