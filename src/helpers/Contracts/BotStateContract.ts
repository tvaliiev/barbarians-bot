import { Page } from 'puppeteer'
import { HandlerTypes } from '../Pipeline'

export interface BotStateContract<T> {
  run(carry: T): Promise<any>

  shouldRun(): Promise<boolean>

  setGlobalHandlers(handlers: HandlerTypes<T>[]): void

  init(page: Page): Promise<any>
}
