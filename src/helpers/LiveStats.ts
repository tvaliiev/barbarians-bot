import { Point, WriteApi } from '@influxdata/influxdb-client'
import { inject, interfaces } from 'inversify'
import pino from 'pino'
import { Page } from 'puppeteer'
import { container } from '../container'
import { TradingOptions } from '../core/bots/TraderBot'
import { PageParser } from '../core/PageInteraction/PageParser'
import { Bucket } from './constants/Bucket'
import { ContainerTypes } from './ContainerValues'
import { DamageStatContract, DamageType } from './Contracts/DamageStatContract'
import { IEnvironment } from './environment'
import { FixedSizeArray } from './FixedSizeArray'
import { provideSingleton } from './functions/provideSingleton'
import { parseTime } from './functions/time/ParseTime'
import { Storage } from './Storage'
import SimpleFactory = interfaces.SimpleFactory
import Logger = pino.Logger

export interface LiveStatsContract {
  tradingOptions: TradingOptions | null
  tradingOptionsLastUpdate: number | null
  tradingOptionsHistory: TradingOptionsHistoryItem[]
  damageStats: { [key in DamageType]: DamageStatContract[] }
}

export type TradingOptionsHistoryItem = { tradingOptions: TradingOptions; time: number }

@provideSingleton(LiveStats)
export class LiveStats {
  public tradingOptionsHistory: FixedSizeArray<TradingOptionsHistoryItem> =
    new FixedSizeArray<TradingOptionsHistoryItem>(300)
  public latestTradingOptions: TradingOptions | null = null
  public tradingOptionsLastUpdate: number | null = null
  public lastDamageStat: { [key in DamageType]: FixedSizeArray<DamageStatContract> } = {
    [DamageType.HEAL]: new FixedSizeArray(100),
    [DamageType.ATTACK]: new FixedSizeArray(100),
    [DamageType.BURN_ENERGY]: new FixedSizeArray(100),
    [DamageType.MISS]: new FixedSizeArray(5),
  }
  protected readonly dbClients: { [Bucket.ACTIONS]: WriteApi; [Bucket.TRADING]: WriteApi }
  private lastDamageTime: { [key in DamageType]: number | null } = {
    [DamageType.ATTACK]: null,
    [DamageType.BURN_ENERGY]: null,
    [DamageType.HEAL]: null,
    [DamageType.MISS]: null,
  }

  constructor(
    private readonly storage: Storage,
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
    @inject(ContainerTypes.INFLUXDB_CLIENT)
    readonly dbClientFactory: SimpleFactory<WriteApi, [Bucket]>,
  ) {
    this.dbClients = {
      [Bucket.ACTIONS]: dbClientFactory(Bucket.ACTIONS),
      [Bucket.TRADING]: dbClientFactory(Bucket.TRADING),
    }
    this.restoreFromStorage()
  }

  public async updateTradingOptions(options: TradingOptions): Promise<void> {
    this.latestTradingOptions = options
    this.tradingOptionsLastUpdate = Date.now()
    if (
      !this.tradingOptionsHistory.getLast() ||
      // @ts-ignore
      this.tradingOptionsLastUpdate - this.tradingOptionsHistory.getLast().time > parseTime('10m')
    ) {
      this.tradingOptionsHistory.push({
        tradingOptions: this.latestTradingOptions,
        time: this.tradingOptionsLastUpdate,
      })
    }

    const points: Point[] = []
    //@ts-ignore
    Object.keys(options).forEach((material: keyof TradingOptions) => {
      points.push(
        new Point(`${material}-trading-stats`)
          .floatField('minCostPerItem', options[material]?.min)
          .floatField('maxCostPerItem', options[material]?.max)
          .floatField('avgCostPerItem', options[material]?.avg),
      )
    })
    this.dbClients[Bucket.TRADING].writePoints(points)
    await this.dbClients[Bucket.TRADING].flush()
    this.updateStorage()
  }

  public async pushDamageStat(damageStat: DamageStatContract) {
    const environment = container.get<IEnvironment>(ContainerTypes.ENVIRONMENT)
    const point = new Point('damageStats')
      .tag('type', damageStat.type)
      .tag('account', environment.ACCOUNT_LOGIN)
      .intField('totalDamage', damageStat.damage)
      .booleanField('isCritical', damageStat.isCrit)
      .intField('delay', damageStat.delay)
    try {
      this.dbClients[Bucket.ACTIONS].writePoint(point)
      await this.dbClients[Bucket.ACTIONS].flush()
    } catch (e) {}

    this.lastDamageStat[damageStat.type].push(damageStat)
    this.updateStorage()
  }

  private updateStorage() {
    this.storage.liveStats = this.getAll()
  }

  public getAll(): LiveStatsContract {
    return {
      tradingOptions: this.latestTradingOptions,
      tradingOptionsLastUpdate: this.tradingOptionsLastUpdate,
      damageStats: {
        [DamageType.HEAL]: this.lastDamageStat[DamageType.HEAL].getItems(),
        [DamageType.ATTACK]: this.lastDamageStat[DamageType.ATTACK].getItems(),
        [DamageType.BURN_ENERGY]: this.lastDamageStat[DamageType.BURN_ENERGY].getItems(),
        [DamageType.MISS]: this.lastDamageStat[DamageType.MISS].getItems(),
      },
      tradingOptionsHistory: this.tradingOptionsHistory.getItems(),
    }
  }

  public async logDamage(page: Page): Promise<void> {
    const damageStringToTypeMap: { [key: string]: DamageType } = {
      ['Ты ударил']: DamageType.ATTACK,
      ['Ты полечил']: DamageType.HEAL,
      ['Ты сжёг']: DamageType.BURN_ENERGY,
      ['Ты промахнулся']: DamageType.MISS,
      ['Лечить некого']: DamageType.MISS,
      ['Ты убил']: DamageType.MISS,
      ['Ты получил']: DamageType.MISS,
      ['ударил']: DamageType.MISS,
    }

    for (const damageString in damageStringToTypeMap) {
      const damageLogMessage = await PageParser.getLastLogMessageIfContains(page, damageString)
      if (!damageLogMessage) {
        continue
      }

      const damageType = damageStringToTypeMap[damageString]
      if (damageType === DamageType.MISS) {
        return
      }

      const damageAmountString = damageLogMessage.match(/\d+/)
      if (!damageAmountString) {
        throw new Error(`cant parse damage from string "${damageLogMessage}" (damage type = ${damageType})`)
      }

      const damage = parseInt(damageAmountString[0])
      const logResult = await this.logDamageType(damage, damageType, damageLogMessage.endsWith('крит'))
      if (!logResult) {
        continue
      }

      return
    }

    this.logger.warn({
      message: 'Cant parse damage - out of ideas what it is',
      currentUrl: page.url(),
    })
  }

  private async logDamageType(damage: number, damageType: DamageType, isCrit: boolean): Promise<boolean> {
    const lastDamageTime = this.lastDamageTime[damageType]

    this.lastDamageTime[damageType] = Date.now()
    if (lastDamageTime === null) {
      return true
    }
    const delay = Date.now() - lastDamageTime
    if (delay >= this.storage.actionDelayWithGlobal * 2) {
      return true
    }

    await this.pushDamageStat({
      type: damageType,
      damage,
      delay,
      isCrit,
    })
    return true
  }

  private restoreFromStorage() {
    const storageData = this.storage.liveStats
    if (!storageData) {
      return
    }

    this.latestTradingOptions = storageData.tradingOptions
    if (storageData.damageStats && storageData.damageStats[DamageType.HEAL]) {
      Object.keys(storageData.damageStats).forEach((key) => {
        // @ts-ignore
        this.lastDamageStat[key].push(...storageData.damageStats[key])
      })
    }
    if (storageData.tradingOptionsHistory) {
      this.tradingOptionsHistory.push(...storageData.tradingOptionsHistory)
    }
    this.tradingOptionsLastUpdate = storageData.tradingOptionsLastUpdate
  }
}
