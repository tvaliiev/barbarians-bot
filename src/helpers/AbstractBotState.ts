import { injectable } from 'inversify'
import { Page } from 'puppeteer'
import { BotStateContract } from './Contracts/BotStateContract'
import { HandlerTypes, Pipeline } from './Pipeline'

@injectable()
export abstract class AbstractBotState<T> implements BotStateContract<T> {
  protected globalHandlers: HandlerTypes<T>[] = []

  public setGlobalHandlers(handlers: HandlerTypes<T>[]) {
    this.globalHandlers = handlers
  }

  public getGlobalHandlers(): HandlerTypes<T>[] {
    return this.globalHandlers
  }

  public abstract run(carry: T): Promise<any>

  public abstract shouldRun(): Promise<boolean>

  public async init(page: Page): Promise<any> {}

  protected async runWithGlobalHandlers(carry: T, handlers: HandlerTypes<T>[]): Promise<T> {
    return new Pipeline<T>()
      .send(carry)
      .through([...this.getGlobalHandlers(), ...handlers])
      .thenReturn()
  }
}
