import { inject, injectable, postConstruct } from 'inversify'
import pino from 'pino'
import { Page } from 'puppeteer'
import { container } from '../../container'
import { delayMiddleware } from '../../handlers/Global/DelayMiddleware'
import { HandleBrokenGearMiddleware } from '../../handlers/Global/HandleBrokenGearMiddleware'
import { HandleHasAltarBuffMiddleware } from '../../handlers/Global/HandleHasAltarBuffMiddleware'
import { HandleHasQuestMiddleware } from '../../handlers/Global/HandleHasQuestMiddleware'
import { HandleStuffOnHeroPageMiddleware } from '../../handlers/Global/HandleStuffOnHeroPageMiddleware'
import { HandleUpdateHeroGlobalStatsMiddleware } from '../../handlers/Global/HandleUpdateHeroGlobalStatsMiddleware'
import { HandleBetterGearInBackPackMiddleware } from '../../handlers/MedicInTower/HandleBetterGearInBackPackMiddleware'
import { HandleClanInvitationPopupMiddleware } from '../../handlers/MedicInTower/HandleClanInvitationPopupMiddleware'
import { HandleFullBackpackMiddleware } from '../../handlers/MedicInTower/HandleFullBackpackMiddleware'
import { HandlePutOnBetterThingPopupMiddleware } from '../../handlers/MedicInTower/HandlePutOnBetterThingPopupMiddleware'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { BotStateContract } from '../../helpers/Contracts/BotStateContract'
import { HandlerTypes } from '../../helpers/Pipeline'
import { Storage } from '../../helpers/Storage'
import { BotState } from '../BotState'
import { Hero } from '../Hero'
import { PageParser } from '../PageInteraction/PageParser'
import { ClanQuest } from '../PageInteraction/PageParsers/ClanQuestsPageParser'
import { ArenaFighterBotState } from './FighterBotStates/ArenaFighterBotState'
import { BattlefieldsFighterBotState } from './FighterBotStates/BattlefieldsFighterBotState'
import { DuelsFighterBotState } from './FighterBotStates/DuelsFighterBotState'
import { DungeonsFighterBotState } from './FighterBotStates/DungeonsFighterBotState'
import { NoopFighterBotState } from './FighterBotStates/NoopFighterBotState'
import { OgreDungeonFighterBotState } from './FighterBotStates/OgreDungeonFighterBotState'
import { SurvivalFighterBotState } from './FighterBotStates/SurvivalFighterBotState'
import { TowersFighterBotState } from './FighterBotStates/TowersFighterBotState'
import { GenericBot } from './GenericBot'
import Logger = pino.Logger

export type FighterBotCarry = { hero: Hero; page: Page }

export enum PrioritiesMode {
  IRON_HUNGRY = 'iron hungry',
  QUESTS = 'quests',
  GENERAL = 'general',
}

export interface FighterBotStateContract extends BotStateContract<FighterBotCarry> {}

@injectable()
export class FighterBot extends GenericBot {
  // @ts-ignore
  protected stateHandlers: { [key in BotState]: FighterBotStateContract }
  protected currentState: BotState = BotState.NOOP

  constructor(
    @inject(ContainerTypes.PAGE)
    protected readonly page: Page,
    protected readonly hero: Hero,
    protected readonly storage: Storage,
    @inject(ContainerTypes.LOGGER)
    protected readonly logger: Logger,
  ) {
    super()
  }

  protected static async getStateHandlers(): Promise<{
    [key in BotState]: FighterBotStateContract
  }> {
    return {
      [BotState.ARENA]: await container.getAsync(ArenaFighterBotState),
      [BotState.DUNGEONS]: await container.getAsync(DungeonsFighterBotState),
      [BotState.SURVIVAL]: await container.getAsync(SurvivalFighterBotState),
      [BotState.TOWER]: await container.getAsync(TowersFighterBotState),
      [BotState.DUELS]: await container.getAsync(DuelsFighterBotState),
      [BotState.OGRE_DUNGEON]: await container.getAsync(OgreDungeonFighterBotState),
      [BotState.NOOP]: await container.getAsync(NoopFighterBotState),
      [BotState.BATTLEFIELDS]: await container.getAsync(BattlefieldsFighterBotState),
    }
  }

  protected async handleError(error: any) {
    const { path: imagePath } = await PageParser.screenshot(this.page)
    this.logger.error({
      err: error,
      currentUrl: this.page.url(),
      image: imagePath,
    })

    if (error.name === 'TimeoutError') {
      await this.page.reload()
      return
    }
    // TODO: when bot is reliable, we should probably just reset it
    this.stop()
  }

  protected static async getGlobalHandlers(): Promise<HandlerTypes<FighterBotCarry>[]> {
    return [
      delayMiddleware(container.get(Storage).globalDelay),
      await container.getAsync(HandleClanInvitationPopupMiddleware),
      await container.getAsync(HandleUpdateHeroGlobalStatsMiddleware),
      await container.getAsync(HandleHasAltarBuffMiddleware),
      await container.getAsync(HandlePutOnBetterThingPopupMiddleware),
      await container.getAsync(HandleBetterGearInBackPackMiddleware),
      await container.getAsync(HandleFullBackpackMiddleware),
      await container.getAsync(HandleStuffOnHeroPageMiddleware),
      await container.getAsync(HandleBrokenGearMiddleware),
      await container.getAsync(HandleHasQuestMiddleware),
    ]
  }

  protected async eachTick(): Promise<void> {
    const previousState = this.currentState
    await this.switchStateIfNeeded()

    if (previousState !== this.currentState) {
      this.logger.info(`Switching fighter state from %s to %s`, previousState, this.currentState)
    }

    await this.stateHandlers[this.currentState].run(this.getCarry())
  }

  @postConstruct()
  protected async init(): Promise<void> {
    await this.setStateHandlers()
  }

  protected async getPriorities(): Promise<BotState[]> {
    const priorityConfigs: {
      [key in BotState]: {
        minimumLevel: number
        ironEfficient?: boolean
        questPriority?: ClanQuest
        if?: (hero: Hero) => Promise<boolean>
      }
    } = {
      [BotState.NOOP]: { minimumLevel: 1 },
      [BotState.OGRE_DUNGEON]: { minimumLevel: 1 },
      [BotState.DUELS]: { minimumLevel: 1, ironEfficient: true },
      [BotState.TOWER]: { minimumLevel: 1, if: async (hero: Hero) => hero.fatigue.amount < 35 },
      [BotState.DUNGEONS]: { minimumLevel: 10, ironEfficient: true },
      [BotState.SURVIVAL]: { minimumLevel: 25, questPriority: ClanQuest.SURVIVAL },
      [BotState.ARENA]: { minimumLevel: 10 },
      [BotState.BATTLEFIELDS]: { minimumLevel: 40 },
    }

    const basePriorities = await this.getBasePriorities()
    const needIronHeavily = await this.hero.needIronHeavily(this.page)
    const priorities: BotState[] = []
    for (let i = 0; i < basePriorities.length; i++) {
      const state = basePriorities[i]
      const priorityConfig = priorityConfigs[state]
      if (priorityConfig.minimumLevel > this.hero.level) {
        continue
      }

      if (priorityConfig.if && !(await priorityConfig.if(this.hero))) {
        continue
      }

      if (needIronHeavily && !priorityConfig.ironEfficient) {
        continue
      }

      if (this.hero.activeQuest && this.hero.activeQuest.name === priorityConfig.questPriority) {
        priorities.unshift(state)
        continue
      }

      priorities.push(state)
    }

    return priorities
  }

  protected async getBasePriorities(): Promise<BotState[]> {
    return [
      BotState.DUELS,
      BotState.OGRE_DUNGEON,
      BotState.DUNGEONS,
      BotState.SURVIVAL,
      // TODO: WIP battlefields
      // BotState.BATTLEFIELDS,
      BotState.TOWER,
      BotState.ARENA,
    ]
  }

  protected getCarry(): FighterBotCarry {
    return { hero: this.hero, page: this.page }
  }

  protected async setStateHandlers() {
    this.stateHandlers = await FighterBot.getStateHandlers()
    for (const stateHandler of Object.values(this.stateHandlers)) {
      stateHandler.setGlobalHandlers(await FighterBot.getGlobalHandlers())
      await stateHandler.init(this.page)
    }
  }

  protected async switchStateIfNeeded() {
    const statePriorities = await this.getPriorities()
    for (let i = 0; i < statePriorities.length; i++) {
      const state = statePriorities[i]

      if (await this.stateHandlers[state].shouldRun()) {
        this.currentState = state
        return
      }
    }

    this.currentState = BotState.NOOP
  }
}
