import { inject, injectable } from 'inversify'
import { Page } from 'puppeteer'
import { container } from '../../container'
import { ContainerTypes } from '../../helpers/ContainerValues'
import delay from '../../helpers/functions/delay'
import { HandlerTypes, Pipeline } from '../../helpers/Pipeline'
import { Storage } from '../../helpers/Storage'
import { Hero } from '../Hero'
import { FighterBotCarry, FighterBotStateContract } from './FighterBot'
import { TowersFighterBotState } from './FighterBotStates/TowersFighterBotState'
import { GenericBot } from './GenericBot'

@injectable()
export class TestBot extends GenericBot {
  constructor(
    @inject(ContainerTypes.PAGE)
    private readonly page: Page,
    private readonly hero: Hero,
    private readonly storage: Storage,
  ) {
    super()
  }

  protected async eachTick(): Promise<void> {
    await this.runFighterBotState(await container.getAsync(TowersFighterBotState))

    await delay(this.storage.globalDelay)
  }

  protected async runFighterBotState(state: FighterBotStateContract): Promise<void> {
    await state.run(this.getFighterBotCarry())
  }

  protected async runFighterHandlers(handlers: HandlerTypes<FighterBotCarry>[]): Promise<void> {
    await new Pipeline<FighterBotCarry>().send(this.getFighterBotCarry()).through(handlers).thenReturn()
  }

  protected async runBotTick(bot: GenericBot): Promise<void> {
    //@ts-ignore
    await bot.eachTick()
  }

  private getFighterBotCarry(): FighterBotCarry {
    return { page: this.page, hero: this.hero }
  }

  protected async handleError(error: any): Promise<void> {
    console.error(error)
  }
}
