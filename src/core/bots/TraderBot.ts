import { inject, injectable } from 'inversify'
import pino from 'pino'
import { Page } from 'puppeteer'
import { container } from '../../container'
import { delayMiddleware } from '../../handlers/Global/DelayMiddleware'
import { BindTradingInfoForStatsPage } from '../../handlers/Trader/BindTradingInfoForStatsPage'
import { InjectTradingInfoMiddleware } from '../../handlers/Trader/InjectTradingInfoMiddleware'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { Pipeline } from '../../helpers/Pipeline'
import { GenericBot } from './GenericBot'
import Logger = pino.Logger

export enum TradingTypes {
  IRON = 'iron',
  POTIONS = 'potions',
  MIFRIL = 'mifril',
}

export type TradingOption = {
  type: TradingTypes
  costTotal: number
  costPerItem: number
  slotNumber: number
  amount: number
  page: number
}

export enum TradingOptionStatType {
  MIN_COST_PER_ITEM = 'min',
  MAX_COST_PER_ITEM = 'max',
  AVG_COST_PER_ITEM = 'avg',
}

export type TradingOptions = {
  [key in TradingTypes]?: {
    [key in TradingOptionStatType]: TradingOption
  }
}
export type TradingInfo = {
  tradingOptions: TradingOptions
}
export type TradingBotCarry = TradingInfo & { mainPage: Page }

@injectable()
export class TraderBot extends GenericBot {
  constructor(
    @inject(ContainerTypes.PAGE)
    private mainPage: Page,
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
  ) {
    super()
  }

  public stop() {
    super.stop()
  }

  protected async eachTick(): Promise<void> {
    await new Pipeline<TradingBotCarry>()
      .send({
        mainPage: this.mainPage,
        tradingOptions: {},
      })
      .through([
        delayMiddleware(0, 20000),
        container.get(InjectTradingInfoMiddleware),
        container.get(BindTradingInfoForStatsPage),
      ])
      .thenReturn()
  }

  protected async handleError(error: any): Promise<void> {
    this.logger.error(error)
  }
}
