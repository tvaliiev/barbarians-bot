import * as http from 'http'
import { Server } from 'http'
import { inject, injectable, interfaces, postConstruct } from 'inversify'
import { RenderFunction } from '../../containers/libs'
import { ContainerTypes } from '../../helpers/ContainerValues'
import delay from '../../helpers/functions/delay'
import { LiveStats } from '../../helpers/LiveStats'
import { GenericBot } from './GenericBot'
import Factory = interfaces.Factory

@injectable()
export class StatsServerBot extends GenericBot {
  // @ts-ignore
  private server: Server
  private readonly template: RenderFunction

  constructor(
    @inject(ContainerTypes.TEMPLATE_FACTORY)
    template: Factory<RenderFunction>,
    private liveStats: LiveStats,
  ) {
    super()
    this.template = template('stats-page') as RenderFunction
  }

  @postConstruct()
  public async postCreate(): Promise<void> {
    this.server = http
      .createServer(async (req, res) => {
        if (req.url === '/') {
          res.writeHead(200, { 'Content-Type': 'text/html' })
          res.write(
            this.template({
              '%options%': this.liveStats.getAll(),
            }),
          )
        } else {
          res.writeHead(200, { ['Content-Type']: 'application/json' })
          res.write(JSON.stringify(this.liveStats.getAll()))
        }

        res.end()
      })
      .listen(2020)
  }

  public stop() {
    super.stop()
    this.server.emit('close')
  }

  protected async eachTick(): Promise<void> {
    await delay(30000)
  }

  protected async handleError(error: any): Promise<void> {}
}
