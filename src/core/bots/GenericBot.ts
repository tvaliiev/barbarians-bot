import { injectable } from 'inversify'

@injectable()
export abstract class GenericBot {
  protected shouldStop = false

  public async run(): Promise<void> {
    while (!this.shouldStop) {
      try {
        await this.eachTick()
      } catch (error: any) {
        await this.handleError(error)
      }
    }
  }

  public stop(): void {
    this.shouldStop = true
  }

  protected abstract handleError(error: any): Promise<void>

  protected abstract eachTick(): Promise<void>
}
