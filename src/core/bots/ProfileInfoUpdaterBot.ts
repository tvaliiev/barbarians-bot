import { inject, injectable } from 'inversify'
import pino from 'pino'
import { Page } from 'puppeteer'
import { ContainerTypes } from '../../helpers/ContainerValues'
import delay from '../../helpers/functions/delay'
import { Hero } from '../Hero'
import { GenericBot } from './GenericBot'
import Logger = pino.Logger

@injectable()
export class ProfileInfoUpdaterBot extends GenericBot {
  constructor(
    private hero: Hero,
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
    @inject(ContainerTypes.PAGE)
    private page: Page,
  ) {
    super()
  }

  protected async eachTick(): Promise<void> {
    await delay(5000)

    await this.hero.updateDynamicInfoFromHeroPage(this.page)
    await this.hero.updateInfoFromProfilePage(this.page)
    await this.hero.updateGlobalInfo(this.page)
    await this.hero.updateQuestProgress(this.page)
    await this.hero.collectAllQuestRewards(this.page)
    if (!this.hero.isLowIron()) {
      await this.hero.gatherExpFromIronMaster(this.page)
    }
  }

  protected async handleError(error: any): Promise<void> {
    this.logger.error(error)
  }
}
