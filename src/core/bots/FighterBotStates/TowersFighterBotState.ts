import { container } from '../../../container'
import { HandlePutProperSetMiddleware } from '../../../handlers/Global/HandlePutProperSetMiddleware'
import { HandleBurnEnergyAttackMiddleware } from '../../../handlers/MedicInTower/HandleBurnEnergyAttackMiddleware'
import { HandleLowEnergyMiddleware } from '../../../handlers/MedicInTower/HandleLowEnergyMiddleware'
import { HandleLowHPMiddleware } from '../../../handlers/MedicInTower/HandleLowHPMiddleware'
import { HandleTowerWithEnemiesLocationMiddleware } from '../../../handlers/Walker/HandleTowerWithEnemiesLocationMiddleware'
import { AbstractBotState } from '../../../helpers/AbstractBotState'
import { provideSingleton } from '../../../helpers/functions/provideSingleton'
import { parseTime } from '../../../helpers/functions/time/ParseTime'
import { Hero } from '../../Hero'
import { SetNames } from '../../PageInteraction/PageParsers/SetsPageParser'
import { FighterBotCarry, FighterBotStateContract } from '../FighterBot'
import { UpdateBattleLogMiddleware } from '../../../handlers/Global/UpdateBattleLogMiddleware'

@provideSingleton(TowersFighterBotState)
export class TowersFighterBotState extends AbstractBotState<FighterBotCarry> implements FighterBotStateContract {
  constructor(private hero: Hero) {
    super()
  }

  public async run(carry: FighterBotCarry): Promise<any> {
    return this.runWithGlobalHandlers(carry, [
      HandlePutProperSetMiddleware(SetNames.TOWERS),

      container.get(HandleLowEnergyMiddleware),

      container.get(HandleTowerWithEnemiesLocationMiddleware),

      container.get(HandleLowHPMiddleware),
      container.get(HandleBurnEnergyAttackMiddleware),

      container.get(UpdateBattleLogMiddleware),
    ])
  }

  public async shouldRun(): Promise<boolean> {
    const isWearsOffSoon = this.hero.fatigue.amount > 0 && this.hero.fatigue.wearsOffAt - Date.now() < parseTime('3h')
    if (isWearsOffSoon) {
      return false
    }

    return this.hero.fatigue.amount < 70 || this.hero.iron < this.hero.altarBuff.price
  }
}
