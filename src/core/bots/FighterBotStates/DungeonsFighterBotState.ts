import { Page } from 'puppeteer'
import { container } from '../../../container'
import { HandleFightWithCommonLinksMiddleware } from '../../../handlers/Fighter/HandleFightWithCommonLinksMiddleware'
import { HandlePutProperSetMiddleware } from '../../../handlers/Global/HandlePutProperSetMiddleware'
import { UpdateDungeonAvailability } from '../../../handlers/Global/UpdateDungeonAvailability'
import { HandleDungeonWaitingRoomMiddleware } from '../../../handlers/Waiter/HandleDungeonWaitingRoomMiddleware'
import { WaitUntilRefreshButtonDisappear } from '../../../handlers/Waiter/WaitUntilRefreshButtonDisappearMiddleware'
import { AbstractBotState } from '../../../helpers/AbstractBotState'
import { provideSingleton } from '../../../helpers/functions/provideSingleton'
import { parseTime } from '../../../helpers/functions/time/ParseTime'
import { Storage } from '../../../helpers/Storage'
import { SetNames } from '../../PageInteraction/PageParsers/SetsPageParser'
import { FighterBotCarry, FighterBotStateContract } from '../FighterBot'

@provideSingleton(DungeonsFighterBotState)
export class DungeonsFighterBotState extends AbstractBotState<FighterBotCarry> implements FighterBotStateContract {
  constructor(private storage: Storage) {
    super()
  }

  public async run(carry: FighterBotCarry): Promise<any> {
    return this.runWithGlobalHandlers(carry, [
      HandlePutProperSetMiddleware(SetNames.FIGHT),
      container.get(HandleDungeonWaitingRoomMiddleware),
      WaitUntilRefreshButtonDisappear({
        timeoutInMs: parseTime(carry.hero.isLowIron() ? '5m' : '1m'),
        onFail: async () => this.storage.postponeDungeonFight(),
      }),
      container.get(HandleFightWithCommonLinksMiddleware),
      container.get(UpdateDungeonAvailability),
    ])
  }

  public async init(page: Page): Promise<any> {
    await UpdateDungeonAvailability.updateNextAvailableDungeonTime(page)
  }

  public async shouldRun(): Promise<boolean> {
    return this.storage.dungeonAvailableAt <= Date.now()
  }
}
