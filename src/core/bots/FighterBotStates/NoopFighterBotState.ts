import { delayMiddleware } from '../../../handlers/Global/DelayMiddleware'
import { AbstractBotState } from '../../../helpers/AbstractBotState'
import { provideSingleton } from '../../../helpers/functions/provideSingleton'
import { followLink } from '../../PageInteraction/Navigation'
import { FighterBotCarry, FighterBotStateContract } from '../FighterBot'

@provideSingleton(NoopFighterBotState)
export class NoopFighterBotState extends AbstractBotState<FighterBotCarry> implements FighterBotStateContract {
  public async run(carry: FighterBotCarry): Promise<any> {
    return this.runWithGlobalHandlers(carry, [
      delayMiddleware(10000),
      async (carry, next) => {
        await followLink(carry.page, '/')
        return next(carry)
      },
    ])
  }

  public async shouldRun(): Promise<boolean> {
    return true
  }
}
