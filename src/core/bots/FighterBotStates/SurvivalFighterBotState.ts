import { container } from '../../../container'
import { HandleFightWithCommonLinksMiddleware } from '../../../handlers/Fighter/HandleFightWithCommonLinksMiddleware'
import { HandlePutProperSetMiddleware } from '../../../handlers/Global/HandlePutProperSetMiddleware'
import { WaitUntilRefreshButtonDisappear } from '../../../handlers/Waiter/WaitUntilRefreshButtonDisappearMiddleware'
import { ArenaType, handleInArenaAndJoinedMiddleware } from '../../../handlers/Walker/HandleInArenaAndJoinedMiddleware'
import { AbstractBotState } from '../../../helpers/AbstractBotState'
import { LinkName } from '../../../helpers/constants/LinkName'
import { provideSingleton } from '../../../helpers/functions/provideSingleton'
import { parseTime } from '../../../helpers/functions/time/ParseTime'
import { Storage } from '../../../helpers/Storage'
import { ClanQuest } from '../../PageInteraction/PageParsers/ClanQuestsPageParser'
import { SetNames } from '../../PageInteraction/PageParsers/SetsPageParser'
import { FighterBotCarry, FighterBotStateContract } from '../FighterBot'

@provideSingleton(SurvivalFighterBotState)
export class SurvivalFighterBotState extends AbstractBotState<FighterBotCarry> implements FighterBotStateContract {
  constructor(private storage: Storage) {
    super()
  }

  public async run(carry: FighterBotCarry): Promise<any> {
    const isInSurvivalQuest = carry.hero.activeQuest && carry.hero.activeQuest.name === ClanQuest.SURVIVAL
    return this.runWithGlobalHandlers(carry, [
      HandlePutProperSetMiddleware(SetNames.FIGHT),
      handleInArenaAndJoinedMiddleware(ArenaType.SURVIVAL),
      WaitUntilRefreshButtonDisappear({
        timeoutInMs: parseTime('30s'),
        onFail: async () => {
          if (isInSurvivalQuest) {
            return
          }

          this.storage.postponeSurvivalFight()
        },
      }),
      container.get(HandleFightWithCommonLinksMiddleware),
      WaitUntilRefreshButtonDisappear({
        timeoutInMs: 0,
        linkAppearance: [LinkName.NEW_FIGHT],
      }),
      async (carry, next) => {
        if (!isInSurvivalQuest) {
          this.storage.postponeSurvivalFight()
        }

        return next(carry)
      },
    ])
  }

  public async shouldRun(): Promise<boolean> {
    return this.storage.survivalAvailableAt <= Date.now()
  }
}
