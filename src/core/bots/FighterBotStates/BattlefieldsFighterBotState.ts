import { Page } from 'puppeteer'
import { container } from '../../../container'
import { HandleBattlefieldsFightMiddleware } from '../../../handlers/Fighter/HandleBattlefieldsFightMiddleware'
import { HandlePutProperSetMiddleware } from '../../../handlers/Global/HandlePutProperSetMiddleware'
import { WaitUntilRefreshButtonDisappear } from '../../../handlers/Waiter/WaitUntilRefreshButtonDisappearMiddleware'
import { HandleInBattlefieldsAndJoinedMiddleware } from '../../../handlers/Walker/HandleInBattlefieldsAndJoinedMiddleware'
import { AbstractBotState } from '../../../helpers/AbstractBotState'
import { provideSingleton } from '../../../helpers/functions/provideSingleton'
import { parseTime } from '../../../helpers/functions/time/ParseTime'
import { Storage } from '../../../helpers/Storage'
import { BattleFieldsQueuePageActions } from '../../PageInteraction/PageActions/BattleFieldsQueuePageActions'
import { SetNames } from '../../PageInteraction/PageParsers/SetsPageParser'
import { FighterBotCarry, FighterBotStateContract } from '../FighterBot'

@provideSingleton(BattlefieldsFighterBotState)
export class BattlefieldsFighterBotState extends AbstractBotState<FighterBotCarry> implements FighterBotStateContract {
  constructor(
    private readonly pageActions: BattleFieldsQueuePageActions,
    private readonly storage: Storage,
  ) {
    super()
  }

  public async run(carry: FighterBotCarry): Promise<any> {
    return this.runWithGlobalHandlers(carry, [
      HandlePutProperSetMiddleware(SetNames.FIGHT),
      await container.getAsync(HandleInBattlefieldsAndJoinedMiddleware),
      WaitUntilRefreshButtonDisappear({
        timeoutInMs: parseTime('10m'),
        onFail: async () => await this.init(carry.page),
      }),
      await container.getAsync(HandleBattlefieldsFightMiddleware),
    ])
  }

  public async init(page: Page): Promise<any> {
    this.storage.battlefieldsAvailableAt = await this.pageActions.getRemainingTime(page)
  }

  public async shouldRun(): Promise<boolean> {
    const remainingTime = this.storage.battlefieldsAvailableAt - Date.now()
    if (remainingTime < 0) {
      return false
    }

    return remainingTime < parseTime('7m')
  }
}
