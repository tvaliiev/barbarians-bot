import { container } from '../../../container'
import { HandleFightWithDuelTypeLinksMiddleware } from '../../../handlers/Fighter/HandleFightWithDuelTypeLinksMiddleware'
import { DuelType, HandleInDuelMiddleware } from '../../../handlers/Global/HandleInDuelMiddleware'
import { HandlePutProperSetMiddleware } from '../../../handlers/Global/HandlePutProperSetMiddleware'
import { AbstractBotState } from '../../../helpers/AbstractBotState'
import { provideSingleton } from '../../../helpers/functions/provideSingleton'
import { Storage } from '../../../helpers/Storage'
import { SetNames } from '../../PageInteraction/PageParsers/SetsPageParser'
import { FighterBotCarry, FighterBotStateContract } from '../FighterBot'

@provideSingleton(DuelsFighterBotState)
export class DuelsFighterBotState extends AbstractBotState<FighterBotCarry> implements FighterBotStateContract {
  constructor(private storage: Storage) {
    super()
  }

  public async run(carry: FighterBotCarry): Promise<any> {
    return this.runWithGlobalHandlers(carry, [
      HandlePutProperSetMiddleware(SetNames.FIGHT),
      HandleInDuelMiddleware(DuelType.DUEL),
      await container.get(HandleFightWithDuelTypeLinksMiddleware),
    ])
  }

  public async shouldRun(): Promise<boolean> {
    return this.storage.duelsAvailableAt <= Date.now()
  }
}
