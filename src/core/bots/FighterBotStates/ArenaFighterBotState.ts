import { container } from '../../../container'
import { HandleFightWithCommonLinksMiddleware } from '../../../handlers/Fighter/HandleFightWithCommonLinksMiddleware'
import { HandlePutProperSetMiddleware } from '../../../handlers/Global/HandlePutProperSetMiddleware'
import { WaitUntilRefreshButtonDisappear } from '../../../handlers/Waiter/WaitUntilRefreshButtonDisappearMiddleware'
import { ArenaType, handleInArenaAndJoinedMiddleware } from '../../../handlers/Walker/HandleInArenaAndJoinedMiddleware'
import { AbstractBotState } from '../../../helpers/AbstractBotState'
import { LinkName } from '../../../helpers/constants/LinkName'
import { provideSingleton } from '../../../helpers/functions/provideSingleton'
import { parseTime } from '../../../helpers/functions/time/ParseTime'
import { Storage } from '../../../helpers/Storage'
import { SetNames } from '../../PageInteraction/PageParsers/SetsPageParser'
import { FighterBotCarry, FighterBotStateContract } from '../FighterBot'

@provideSingleton(ArenaFighterBotState)
export class ArenaFighterBotState extends AbstractBotState<FighterBotCarry> implements FighterBotStateContract {
  constructor(private storage: Storage) {
    super()
  }

  public async run(carry: FighterBotCarry): Promise<any> {
    return this.runWithGlobalHandlers(carry, [
      HandlePutProperSetMiddleware(SetNames.FIGHT),
      handleInArenaAndJoinedMiddleware(ArenaType.ARENA),
      WaitUntilRefreshButtonDisappear({
        timeoutInMs: parseTime('2m'),
        onFail: async () => this.storage.postponeArenaFight(),
      }),
      container.get(HandleFightWithCommonLinksMiddleware),
      WaitUntilRefreshButtonDisappear({
        timeoutInMs: 0,
        linkAppearance: [LinkName.NEW_FIGHT],
      }),
      async (carry) => {
        this.storage.postponeArenaFight()
        return carry
      },
    ])
  }

  public async shouldRun(): Promise<boolean> {
    return this.storage.arenaAvailableAt <= Date.now()
  }
}
