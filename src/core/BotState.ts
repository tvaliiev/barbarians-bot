export enum BotState {
  TOWER = 'tower',
  ARENA = 'arena',
  DUNGEONS = 'dungeons',
  SURVIVAL = 'survival',
  DUELS = 'duels',
  OGRE_DUNGEON = 'ogre dungeon',
  NOOP = 'nothing',
  BATTLEFIELDS = 'battlefields',
}
