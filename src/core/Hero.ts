import { inject, postConstruct } from 'inversify'
import pino from 'pino'
import { Page } from 'puppeteer'
import { container } from '../container'
import { AmountParser } from '../helpers/AmountParser'
import { CallbackArray, CallbackMode } from '../helpers/CallbackArray'
import { ContainerTypes } from '../helpers/ContainerValues'
import { provideSingleton } from '../helpers/functions/provideSingleton'
import { parseTime, TimeFormat, TimeOutputFormat } from '../helpers/functions/time/ParseTime'
import { RELATIVE_TIME_REGEX } from '../helpers/functions/time/parseTimeInHumanReadableFormat'
import { SIMPLE_TIME_REGEXP } from '../helpers/functions/time/ParseTimeInSimpleFormat'
import { CallbackHandler } from '../helpers/Pipeline'
import { Storage } from '../helpers/Storage'
import { followLink } from './PageInteraction/Navigation'
import { ClanQuestsPageActions } from './PageInteraction/PageActions/ClanQuestsPageActions'
import { IronMasterPageActions } from './PageInteraction/PageActions/IronMasterPageActions'
import { SetsPageActions } from './PageInteraction/PageActions/SetsPageActions'
import { UserBodyPageActions } from './PageInteraction/PageActions/UserBodyPageActions'
import { UserQuestsPageActions } from './PageInteraction/PageActions/UserQuestsPageActions'
import { ClanQuest, Quest } from './PageInteraction/PageParsers/ClanQuestsPageParser'
import { SetNames } from './PageInteraction/PageParsers/SetsPageParser'
import Logger = pino.Logger
import { BattleLogs } from '../helpers/LogEntries/BattleLogs'

export type HeroLevelUpCallback = CallbackHandler<Hero>

@provideSingleton(Hero)
export class Hero {
  // Stats that can be seen globally
  public health: number = -1
  public energy: number = -1
  public experience: number = -1
  // Stats that can only be seen on profile page
  public maxEnergy: number = -1
  public maxHealth: number = -1
  public strength: number = -1
  public regeneration: number = -1
  public armor: number = -1
  public armorPercent: number = -1
  public criticalChance: number = -1
  public money: number = -1
  public potions: number = -1
  public iron: number = -1
  public fatigue: { amount: number; wearsOffAt: number } = {
    amount: -1,
    wearsOffAt: 0,
  }
  public inGuild: boolean = false
  public level: number = -1
  public altarBuff: {
    power: number
    availableTill: number
    price: number
  } = {
    power: 0,
    availableTill: 0,
    price: 1450,
  }
  public activeQuest: Quest | null = null
  public battleLogs: BattleLogs = new BattleLogs()
  // Callbacks
  private levelUpCallbacks: CallbackArray<Hero> = new CallbackArray<Hero>()

  constructor(
    @inject(ContainerTypes.LOGGER)
    private readonly logger: Logger,
    private readonly storage: Storage,
    private readonly setsPageActions: SetsPageActions,
    private readonly userBodyPageActions: UserBodyPageActions,
    private readonly clanQuestsPageActions: ClanQuestsPageActions,
    private readonly userQuestsPageActions: UserQuestsPageActions,
    private readonly ironMasterPageActions: IronMasterPageActions,
  ) {}

  public registerLevelUpCallback(key: string, cb: HeroLevelUpCallback, mode: CallbackMode | undefined = undefined) {
    return this.levelUpCallbacks.push(key, cb, mode)
  }

  public async onLevelUp(page: Page) {
    await this.updateAllProfileInfo(page)

    this.logger.info('Executing level up callbacks...')
    await this.levelUpCallbacks.execute(this)
    this.logger.info('Executed level up callbacks')
  }

  @postConstruct()
  public async init() {
    const page = await container.getAsync<Page>(ContainerTypes.PAGE)
    await this.updateAllProfileInfo(page)
    await this.clanQuestsPageActions.initHeroQuestData(this, page)
    await page.close()
  }

  public async shouldUsePotion(): Promise<boolean> {
    return this.health < this.maxHealth * 0.3
  }

  public async updateAllProfileInfo(page: Page) {
    await this.updateInfoFromProfilePage(page)
    await this.updateDynamicInfoFromHeroPage(page)
    await this.updateInfoFromSetsPage(page)
    await this.updateGlobalInfo(page)
  }

  public async updateDynamicInfoFromHeroPage(page: Page) {
    await followLink(page, '/user/tire')

    const fatigueText = (
      await page.evaluate(() => {
        return document.querySelector('span.major')?.textContent || '0'
      })
    ).match(/\d+/)
    if (!fatigueText) {
      this.fatigue.amount = 0
      this.fatigue.wearsOffAt = 0
    } else {
      this.fatigue.amount = parseInt(fatigueText[0])
      const wearsOffText = await page.evaluate(() => document.querySelector('span.notify')?.textContent || '0')
      const wearsOffTimeTextMatch = wearsOffText.match(new RegExp(SIMPLE_TIME_REGEXP))
      if (wearsOffTimeTextMatch) {
        this.fatigue.wearsOffAt = parseTime(
          wearsOffTimeTextMatch[0],
          TimeFormat.SIMPLE,
          TimeOutputFormat.RELATIVE_MILLISECONDS,
        )
      } else {
        this.fatigue.wearsOffAt = Date.now() + parseTime('12h')
      }
    }

    await followLink(page, '/user')

    this.inGuild = !(await page.$('a.flhdr[href="guild/0"]'))
    if (this.inGuild) {
      const altarBuffText = await page.evaluate(
        () => document.querySelector('a[href="guild/altar"]')?.parentElement?.textContent,
      )
      if (altarBuffText) {
        const regex = new RegExp('алтарь \\+(\\d+\\.?\\d?)% \\[(' + RELATIVE_TIME_REGEX + ')\\]', 'g')
        const [matches] = Array.from(altarBuffText.matchAll(regex))
        if (matches) {
          this.altarBuff.power = parseInt(matches[1])
          this.altarBuff.availableTill = parseTime(
            matches[2],
            TimeFormat.HUMAN_READABLE,
            TimeOutputFormat.RELATIVE_MILLISECONDS,
          )
        }
      }
    }

    this.writeHeroToStorage()
  }

  public get currentSet(): SetNames | null {
    return this.setsPageActions.currentSet
  }

  public async refreshGuildAltarBuffIfNeeded(page: Page): Promise<any> {
    if (!this.inGuild) {
      return
    }

    if (this.isLowIron()) {
      return
    }

    if (this.altarBuff.availableTill > Date.now() || this.altarBuff.price > this.iron) {
      return
    }

    await followLink(page, '/guild/altar')
    this.altarBuff.price = await page.evaluate(() => {
      const textContent = document.querySelector('img[src="/images/icons/ironbar.png"]:not(.internalIcon)')
        ?.parentElement?.textContent
      if (!textContent) {
        throw new Error('Cant find altar price')
      }

      const parsedText = textContent.match(/\d+/g)
      if (!parsedText) {
        throw new Error('Cant find number in altar price text')
      }

      return parseInt(parsedText[0])
    })

    let sacrificeIronBtn
    while ((sacrificeIronBtn = await page.$('a[href*="sacrificeIronLink"]'))) {
      await sacrificeIronBtn.click()
      await page.waitForNetworkIdle()
    }

    this.writeHeroToStorage()
  }

  public async updateGlobalInfo(page: Page) {
    const [health, energy] = await page.evaluate(() => {
      let el = document.querySelector('img[src^="/images/icons/life"]')
      if (el?.parentElement?.tagName === 'A') {
        el = null
      }

      if (!el) {
        return [null, null]
      }

      return [
        el?.parentElement?.children[1]?.textContent,
        el?.parentElement?.children[3]?.textContent || el?.parentElement?.children[4]?.textContent,
      ]
    })

    if (health === null || energy === null) {
      this.logger.info('Cant parse health (%s) or energy (%s)', health, energy)
      return
    }

    this.health = parseInt(health || '0')
    this.energy = parseInt(energy || '0')

    const experienceElement = await page.$('.main .cntr div[style^="background:#FFDF8C; height:"]')
    if (experienceElement) {
      const experienceStyleAttributeStr = await experienceElement.evaluate((el) => {
        return el.getAttribute('style')
      })

      const experienceStyleAttribute = experienceStyleAttributeStr
        ?.split(';')
        .reduce<{ [key: string]: string }>((carry, str) => {
          const [key, value] = str.split(':')
          carry[key.trim()] = (value || '').trim()
          return carry
        }, {})

      if (!experienceStyleAttribute) {
        throw new Error('Cannot parse experience bar')
      }
      this.experience = parseInt(experienceStyleAttribute.width || this.experience.toString())
    }

    this.writeHeroToStorage()
  }

  public async chooseSet(page: Page, desiredSet: SetNames) {
    await this.setsPageActions.chooseSet(page, desiredSet)
    this.writeHeroToStorage()
  }

  private writeHeroToStorage() {
    this.storage.hero = {
      health: this.health,
      energy: this.energy,
      experience: this.experience,
      maxEnergy: this.maxEnergy,
      maxHealth: this.maxHealth,
      strength: this.strength,
      regeneration: this.regeneration,
      armor: this.armor,
      armorPercent: this.armorPercent,
      criticalChance: this.criticalChance,
      money: this.money,
      potions: this.potions,
      iron: this.iron,
      fatigue: this.fatigue,
      inGuild: this.inGuild,
      altarBuff: this.altarBuff,
      currentSet: this.currentSet,
      level: this.level,
    }
  }

  private async updateInfoFromSetsPage(page: Page) {
    await this.setsPageActions.upgradeSetsIfNeeded(page, this)
    this.writeHeroToStorage()
  }

  public async updateInfoFromProfilePage(page: Page) {
    await followLink(page, '/user')
    const statsInText = await page.evaluate(() => {
      return document.querySelector('img[src="/images/icons/strength.png"]')?.parentElement?.textContent || ''
    })
    this.logger.debug(statsInText)

    const [[, strength], [, maxHealth], [, maxEnergy], [, regeneration], [, armor]] = [
      ...statsInText.matchAll(/: (\d+)/g),
    ]

    this.strength = parseInt(strength)
    this.maxHealth = parseInt(maxHealth)
    this.maxEnergy = parseInt(maxEnergy)
    this.regeneration = parseInt(regeneration)
    this.armor = parseInt(armor)

    const [[, criticalChance], [, armorPercent]] = [...statsInText.matchAll(/\(([\d.]+)%/g)]

    this.criticalChance = parseFloat(criticalChance)
    this.armorPercent = parseFloat(armorPercent)

    const moneyAndResourcesText = await page.evaluate(
      () => document.querySelector('a[href="payment/base"]')?.parentElement?.textContent || '',
    )

    const [[, moneyGold, moneySilver, potions, iron]] = [
      ...moneyAndResourcesText.matchAll(
        new RegExp(`деньги: (\\d+)\\s?(\\d+)?\\s*ресурсы:\\s+(\\d+)\\s+(${AmountParser.REGEX})`, 'g'),
      ),
    ]
    this.money = moneySilver ? parseInt(moneyGold) * 100 + parseInt(moneySilver) : parseInt(moneyGold)
    this.potions = parseInt(potions)
    this.iron = AmountParser.toNumber(iron)

    const avatarText = await page.evaluate(() => {
      return (
        document.querySelector('a[href^="mnkn/id/"]')?.parentElement?.textContent ||
        document.querySelector('a[href="avatar/my"]')?.parentElement?.parentElement?.textContent
      )
    })

    if (typeof avatarText !== 'string') {
      throw new Error('Cant parse level on hero page')
    }

    const [[, levelText]] = Array.from(avatarText.matchAll(/(\d+) ур,/g))
    this.level = parseInt(levelText)

    this.writeHeroToStorage()
  }

  public async needIronHeavily(page: Page): Promise<number | null> {
    if (!(await this.userBodyPageActions.hasBrokenGear(page))) {
      return null
    }

    const latestUnsuccessfulRepairTime = this.userBodyPageActions.getLatestUnsuccessfulRepairTime()
    if (latestUnsuccessfulRepairTime === 0 || Date.now() - latestUnsuccessfulRepairTime < parseTime('10s')) {
      return null
    }

    return this.userBodyPageActions.getNeededIronAmount()
  }

  public async updateQuestProgress(page: Page): Promise<void> {
    await this.clanQuestsPageActions.updateQuestProgress(page, this)
  }

  public async takeBestQuest(page: Page): Promise<void> {
    await this.clanQuestsPageActions.takeBestQuest(page, this)
  }

  public async collectAllQuestRewards(page: Page): Promise<void> {
    await this.userQuestsPageActions.collectAllRewards(page)
  }

  public async dismissQuest(quest: ClanQuest, page: Page): Promise<void> {
    await this.clanQuestsPageActions.dismissQuest(quest, page, this)
  }

  public async gatherExpFromIronMaster(page: Page): Promise<boolean> {
    return this.ironMasterPageActions.gatherExp(page, this)
  }

  public isLowIron(): boolean {
    return this.iron < 50_000
  }
}
