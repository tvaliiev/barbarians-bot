import pino from 'pino'
import { Page } from 'puppeteer'
import { container } from '../../container'
import { LinkObject } from '../../handlers/HandlerContract'
import { LinkName } from '../../helpers/constants/LinkName'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { AccountProvider, IEnvironment } from '../../helpers/environment'
import { parseTime, TimeFormat, TimeOutputFormat } from '../../helpers/functions/time/ParseTime'
import { RELATIVE_TIME_REGEX } from '../../helpers/functions/time/parseTimeInHumanReadableFormat'
import { Storage } from '../../helpers/Storage'
import { followLink, spacesUrl } from './Navigation'
import Logger = pino.Logger
import { BattleLogEntry } from '../../helpers/LogEntries/BattleLogEntry'
import { BattleLogEntryFactory } from '../../helpers/LogEntries/BattleLogEntryFactory'

export interface PageParserGetLinksOptions {
  useParent?: boolean
}

export type Link = { href: string }
export type DuelAbility = Link & { active: number; cooldown: number }
export type DuelPotion = Link & { left: number; cooldown: number }
export type FightDuelTypePage = {
  attackLastTarget: Link
  abilities: DuelAbility[]
  potion: DuelPotion | null
}

type MainPageContent = {
  duelsAvailableAt: number
  ogreDungeonAvailableAt: number
}

export class PageParser {
  public static getLinks(
    page: Page,
    linkSelector = 'a.flhdr',
    options: PageParserGetLinksOptions = {},
  ): Promise<LinkObject> {
    return page.evaluate(
      (linkSelector, options) => {
        return Array.from(document.querySelectorAll(linkSelector)).reduce((carry: LinkObject, current) => {
          if (!!options.useParent) {
            // @ts-ignore
            current = current.parentElement
          }
          const href = current.getAttribute('href')
          if (current.textContent && href) {
            carry[current.textContent.trim()] = {
              href,
              name: current.textContent.trim(),
            }
          }
          return carry
        }, {})
      },
      linkSelector,
      options,
    ) as Promise<LinkObject>
  }

  public static async parseDungeonsPage(page: Page): Promise<
    {
      href: string | null
      availableAt: number
      relativeTime: number
    }[]
  > {
    await followLink(page, '/game/dungeons/1/1')

    const pageResult = await page.evaluate(() =>
      Array.from(document.querySelectorAll('span[style="color:#FFDF8C"]')).map((el) => {
        if (el.parentElement?.tagName === 'EM') {
          return {
            href: null,
            availableAt:
              el.parentElement.parentElement?.parentElement?.children[2]?.children[0]?.children[0]?.textContent || '',
          }
        }

        return {
          href: el.parentElement?.getAttribute('href') || '',
          availableAt: el?.parentElement?.parentElement?.children[2]?.textContent?.match(/Войти/g) ? null : '',
        }
      }),
    )

    return pageResult
      .filter((e) => e.availableAt !== '')
      .map((e) => ({
        href: e.href,
        availableAt: e.availableAt
          ? parseTime(e.availableAt, TimeFormat.SIMPLE, TimeOutputFormat.RELATIVE_MILLISECONDS)
          : Date.now(),
        relativeTime: e.availableAt ? parseTime(e.availableAt, TimeFormat.SIMPLE, TimeOutputFormat.AS_IS_SECONDS) : 0,
      }))
  }

  public static async followLinkIfExists(
    names: LinkName | LinkName[] | string | string[],
    page: Page,
    linkSelector: string | undefined = undefined,
  ): Promise<boolean> {
    if (!Array.isArray(names)) {
      names = [names]
    }

    const links = await PageParser.getLinks(page, linkSelector)
    const existingName = Object.keys(links).find((pageLinkName) =>
      (names as LinkName[]).find((name) => pageLinkName.startsWith(name)),
    )
    if (!existingName) {
      return false
    }

    await followLink(page, links[existingName].href)
    return true
  }

  public static async isLoggedIn(page: Page): Promise<boolean> {
    const response = await followLink(page, '/user')
    if (response && response.status() > 201) {
      throw new Error(`Barbars game site is not responding properly (${response.status()} - ${response.statusText()})`)
    }
    const element = await page.$('a[href="login"]')
    return element == null
  }

  public static async login(page: Page): Promise<void> {
    const environment = container.get<IEnvironment>(ContainerTypes.ENVIRONMENT)
    const loginHandlers: {
      [key in AccountProvider]: (page: Page) => Promise<void>
    } = {
      [AccountProvider.SPACES]: PageParser.loginInSpaces,
      [AccountProvider.PLAIN]: PageParser.loginInPlain,
    }

    await loginHandlers[environment.ACCOUNT_PROVIDER](page)
  }

  public static async parseDungeonRoomPage(page: Page): Promise<{
    neededWarriors: number
    neededDoctors: number
    actualDoctors: number
    actualWarriors: number
  } | null> {
    const pageText = await page.evaluate(() => {
      const xpath = "//a[contains(text(),'Обновить')]"
      const matchingElement = document.evaluate(
        xpath,
        document,
        null,
        XPathResult.FIRST_ORDERED_NODE_TYPE,
        null,
      ).singleNodeValue
      return matchingElement?.parentElement?.innerText
    })

    if (!pageText) {
      return null
    }

    const [[, actualWarriors, actualDoctors]] = [...pageText.matchAll(/В очереди:\s+(\d+)\s+(\d+)/g)]
    let [[, neededWarriors, neededDoctors, , neededDoctors2]] = [
      ...pageText.matchAll(/Требуются\s+(\d+) воина и\s+(\d+)( или (\d+))? медик/g),
    ]
    neededDoctors = neededDoctors2 || neededDoctors

    return {
      actualDoctors: parseInt(actualDoctors),
      actualWarriors: parseInt(actualWarriors),
      neededDoctors: parseInt(neededDoctors),
      neededWarriors: parseInt(neededWarriors),
    }
  }

  public static async isInDuel(page: Page): Promise<boolean> {
    return !!(await page.$('a[href*="actionPanel:damage"]'))
  }

  public static async parseFightDuelTypePage(page: Page): Promise<FightDuelTypePage> {
    const result = await page.evaluate(() => {
      const attackLastTargetHref = document.querySelector('a[href*="actionPanel:damage"]')?.getAttribute('href')
      const abilities = []

      let i = 0
      let abilityElement
      while ((abilityElement = document.querySelector(`a[href*="abilityCombatPanel:ability:${i}"]`))) {
        const href = abilityElement.getAttribute('href') || ''
        abilityElement = abilityElement.children[0].children
        const abilityCooldownElement = abilityElement[3]
        const active = abilityCooldownElement.classList.contains('buff')
          ? parseInt(Array.from((abilityCooldownElement.textContent || '').matchAll(/\((\d+).*\)/g))[0][1])
          : 0
        const cooldown = abilityCooldownElement.classList.contains('minor')
          ? parseInt(Array.from((abilityCooldownElement.textContent || '').matchAll(/\((\d+).*\)/g))[0][1])
          : 0
        abilities.push({
          href,
          active,
          cooldown,
        })
        i++
      }
      //potion
      let potion = null
      abilityElement = document.querySelector(`a[href*="picturedUsePotionPanel:picturedUsePotionLink"]`)
      if (abilityElement) {
        potion = {
          href: '',
          left: 0,
          cooldown: 0,
        }
        potion.href = abilityElement.getAttribute('href') || ''
        abilityElement = abilityElement.children[0].children
        const abilityCooldownElement = abilityElement[3]
        potion.left = parseInt(Array.from((abilityCooldownElement.textContent || '').matchAll(/\((\d+).*\)/g))[0][1])
      }

      return {
        attackLastTarget: {
          href: attackLastTargetHref || '',
        },
        abilities,
        potion,
      }
    })

    if (!result) {
      throw new Error('Cant parse duel page')
    }

    return result
  }

  public static async parseMainPage(page: Page): Promise<MainPageContent> {
    await followLink(page, '/')

    const pageContent = await page.evaluate(() => document.querySelector('.main')?.textContent)
    if (!pageContent) {
      throw new Error('No content on main page')
    }
    // @ts-ignore
    const result: MainPageContent = {}
    const configs: {
      fieldName: keyof MainPageContent
      mainPageCaption: string
    }[] = [
      { fieldName: 'duelsAvailableAt', mainPageCaption: 'Дуэли' },
      { fieldName: 'ogreDungeonAvailableAt', mainPageCaption: 'Подземелья' },
    ]

    configs.forEach((config) => {
      let availableAt = 0
      const duelsRegex = config.mainPageCaption + '[\\s\\n]+откроются через (' + RELATIVE_TIME_REGEX + ')'
      let [matches] = Array.from(pageContent.matchAll(new RegExp(duelsRegex, 'g')))
      if (matches) {
        availableAt = parseTime(matches[1], TimeFormat.HUMAN_READABLE, TimeOutputFormat.RELATIVE_MILLISECONDS)
      }
      result[config.fieldName] = availableAt
    })

    return result
  }

  public static async updateDuelAndOgreDungeonAvailability(page: Page): Promise<any> {
    const availability = await PageParser.parseMainPage(page)
    const storage = await container.getAsync(Storage)
    storage.ogreDungeonAvailableAt = availability.ogreDungeonAvailableAt
    storage.duelsAvailableAt = availability.duelsAvailableAt
  }

  public static async bgSafeClick(page: Page, selector: string) {
    return page.$eval(selector, (el) => {
      // @ts-ignore
      return el.click()
    })
  }

  public static async getLastLogMessageIfContains(page: Page, substring: string): Promise<string | null> {
    return page.evaluate((substring: string) => {
      const xpath = `//div[contains(text(),'${substring}')]`
      const matchingElement = document.evaluate(
        xpath,
        document,
        null,
        XPathResult.FIRST_ORDERED_NODE_TYPE,
        null,
      ).singleNodeValue

      const textContent = matchingElement?.parentElement?.children[1].textContent
      if (typeof textContent !== 'string') {
        return null
      }

      return textContent.indexOf(substring) === -1 ? null : textContent.trim()
    }, substring)
  }

  private static async loginInPlain(page: Page) {
    await followLink(page, '/login')

    const environment = container.get<IEnvironment>(ContainerTypes.ENVIRONMENT)
    await page.type('[name="login"]', environment.ACCOUNT_LOGIN)
    await page.type('[name="password"]', environment.ACCOUNT_PASSWORD)
    await PageParser.bgSafeClick(page, 'input[type="submit"]')
  }

  private static async loginInSpaces(page: Page) {
    const response = await page.goto(spacesUrl(''), { waitUntil: 'domcontentloaded' })
    if (response && response.status() > 201) {
      throw new Error(`Spaces website response code > 201 (${response.status()} - ${response.statusText()})`)
    }

    const isLoggedIn = !!(await page.$('.block-item__avatar.user__ava_medium.js-my_avatar'))
    const environment = container.get<IEnvironment>(ContainerTypes.ENVIRONMENT)
    const logger = container.get<Logger>(ContainerTypes.LOGGER)

    if (!isLoggedIn) {
      logger.info('Logging in spaces account')
      await page.goto(spacesUrl('/registration/loginform/'), {
        waitUntil: 'domcontentloaded',
      })
      await page.type('#main_content [name="contact"]', environment.ACCOUNT_LOGIN)
      await page.type('#main_content [name="password"]', environment.ACCOUNT_PASSWORD)
      await PageParser.bgSafeClick(page, '#main_content [value="Войти"]')
    }

    await page.waitForSelector('.block-item__avatar.user__ava_medium.js-my_avatar', { timeout: 60_000 })

    await page.goto(spacesUrl('/app/enter/?ent=5'))
  }

  public static async hasUserPageNotifications(page: Page) {
    return await page.evaluate(() => {
      const element = document.querySelector('a.flhdr[href$=user]')
      return element && element.childElementCount > 1
    })
  }

  public static async hasUndistributedTalentPoints(page: Page) {
    await followLink(page, '/')
    await followLink(page, '/user')

    return await page.evaluate(() => {
      return document.querySelector('a[href="user/stances"]')?.parentElement?.childElementCount === 4
    })
  }

  public static async screenshot(page: Page): Promise<{ path: string }> {
    const logDirectory = container.get(ContainerTypes.LOG_DIRECTORY)
    const now = Date.now()
    const imagePath = `${logDirectory}/bot_error_${now}.png`
    await page.screenshot({ path: imagePath })
    return {
      path: imagePath,
    }
  }

  public static async getBattleLog(page: Page): Promise<BattleLogEntry[] | null> {
    const battleLogDOMElements = await page.$x(
      '//a[contains(text(),"Мой герой")]/../preceding-sibling::div/div[last()]/div',
    )
    if (!battleLogDOMElements) return null

    const logs = (await page.evaluate(() => {
      const results = document.evaluate(
        '//a[contains(text(),"Мой герой")]/../preceding-sibling::div/div[last()]/div',
        document,
        null,
        XPathResult.UNORDERED_NODE_ITERATOR_TYPE,
        null,
      )
      const data = []
      let result = null
      while ((result = results.iterateNext())) {
        data.push(result.textContent || '')
      }
      return data
    })) as string[]

    return logs
      .map((e) => e.trim())
      .filter((e) => !!e.trim())
      .reduce((acc: BattleLogEntry[], e: string) => {
        const entry = BattleLogEntryFactory.createFromString(e)
        if (entry) {
          acc.push(entry)
        }

        return acc
      }, [])
  }
}
