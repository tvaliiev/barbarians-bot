import { injectable } from 'inversify'
import { Page } from 'puppeteer'
import { LinkName } from '../../../helpers/constants/LinkName'
import { parseTime, TimeFormat, TimeOutputFormat } from '../../../helpers/functions/time/ParseTime'
import { RELATIVE_TIME_REGEX } from '../../../helpers/functions/time/parseTimeInHumanReadableFormat'
import { PageParser } from '../PageParser'

export enum ClanQuest {
  DRAGON_RAID = 'Рейд на драконов',
  ARENA = 'Кровавая арена',
  SURVIVAL = 'Выжить любой ценой',
  FACE_TO_FACE = 'Лицом к лицу',
  GREAT_ACCOMPLISHMENTS = 'Великие свершения',
  TOURNAMENTS = 'Покорение турниров',
  ENHANCED_EXPERIENCE_GAIN = 'Усиленная прокачка',
}

export type Quest = {
  isAvailable: boolean
  endsAt: number
  startAt: number
  name: ClanQuest
  progress: {
    current: number
    max: number
  }
  links: {
    start: string | null
    end: string | null
  }
}

export type QuestMap = {
  [key in ClanQuest]?: Quest
}
export type ClanQuestsPageData = {
  activeQuest: ClanQuest | null
  availableQuests: QuestMap
}

@injectable()
export class ClanQuestsPageParser {
  public async parsePage(page: Page): Promise<ClanQuestsPageData> {
    await this.ensureOnClanQuestsPage(page)

    return this.parseClanQuestsPage(page)
  }

  private async parseClanQuestsPage(page: Page): Promise<ClanQuestsPageData> {
    const questsIcons = await page.$$('img[src="/images/icons/flag.png"]')
    const result: ClanQuestsPageData = {
      availableQuests: {},
      activeQuest: null,
    }

    for (let i = 0; i < questsIcons.length; i++) {
      const questIcon = questsIcons[i]
      const questName = await questIcon.evaluate((icon) => icon.nextElementSibling?.textContent)
      if (!questName) {
        throw new Error('Cant determine quest name')
      }
      const currentQuest: ClanQuest | undefined = Object.values(ClanQuest).find((v) => v === questName)

      if (!currentQuest) {
        continue
      }

      const questElementText = (await questIcon.evaluate((icon) => icon.parentElement?.textContent)) as string
      const item: Quest = {
        isAvailable: false,
        endsAt: 0,
        startAt: 0,
        name: currentQuest,
        progress: {
          current: 0,
          max: 0,
        },
        links: {
          start: null,
          end: null,
        },
      }

      if (questElementText.match(/Отменить задание/g)) {
        result.activeQuest = currentQuest
        item.links.end =
          (await questIcon.evaluate((icon) => icon.parentElement?.querySelector('a.cbtn')?.getAttribute('href'))) ||
          null
      }

      const progressMatch = [...questElementText.matchAll(/Прогресс: (\d+) из (\d+)/g)]
      if (progressMatch.length) {
        const [, progressCurrent, progressMax] = progressMatch[0]
        item.progress = {
          current: parseInt(progressCurrent),
          max: parseInt(progressMax),
        }
      }

      const remainingMatch = [...questElementText.matchAll(new RegExp('Осталось: (' + RELATIVE_TIME_REGEX + ')', 'g'))]
      if (remainingMatch.length) {
        item.endsAt = parseTime(remainingMatch[0][1], TimeFormat.HUMAN_READABLE, TimeOutputFormat.RELATIVE_MILLISECONDS)
      }

      const startAtMatch = [
        ...questElementText.matchAll(new RegExp('Будет доступно через: (' + RELATIVE_TIME_REGEX + ')', 'g')),
      ]
      if (startAtMatch.length) {
        item.startAt = parseTime(startAtMatch[0][1], TimeFormat.HUMAN_READABLE, TimeOutputFormat.RELATIVE_MILLISECONDS)
      }

      item.isAvailable = !!questElementText.match(/Взять задание/g)
      if (item.isAvailable) {
        item.links.start =
          (await questIcon.evaluate((icon) => icon.parentElement?.querySelector('a')?.getAttribute('href'))) || null
      }
      // @ts-ignore
      result.availableQuests[currentQuest] = item
    }

    return result
  }

  private async ensureOnClanQuestsPage(page: Page) {
    if ((await page.evaluate(() => document.querySelector('h1')?.textContent)) === 'Клановые задания') {
      return
    }

    if (!(await PageParser.followLinkIfExists(LinkName.MY_CLAN, page))) {
      throw new Error('Not in clan')
    }

    if (!(await PageParser.followLinkIfExists(LinkName.CLAN_QUESTS, page, 'a'))) {
      throw new Error('Clan does not have clan quests')
    }
  }
}
