import { injectable } from 'inversify'
import { Page } from 'puppeteer'
import { Link, PageParser } from '../PageParser'

type DonationLinkType = 'iron' | 'gold'
export type IronMasterPageData = {
  donationLinks: {
    [key in DonationLinkType]: { amount: number; link: Link }[]
  }
}

@injectable()
export class IronMasterPageParser {
  public async parsePage(page: Page): Promise<IronMasterPageData> {
    const config: { [key: string]: DonationLinkType } = {
      железа: 'iron',
      золота: 'gold',
    }
    const result: IronMasterPageData = {
      donationLinks: {
        gold: [],
        iron: [],
      },
    }

    const links = await PageParser.getLinks(page, 'a[href*="getExpLink"]')

    Object.keys(links).map((key) => {
      const match = Array.from(key.matchAll(/Пожертвовать (\d+) ([а-я]+)/g))
      if (!match.length) {
        throw new Error('Cannot parse iron master page')
      }
      const [, amount, type] = match[0]
      if (typeof config[type] === 'undefined') {
        throw new Error('Cannot parse iron master page')
      }

      result.donationLinks[config[type]].push({
        amount: parseInt(amount),
        link: links[key],
      })
    })

    return result
  }
}
