import { injectable } from 'inversify'
import { Page } from 'puppeteer'
import { parseTime, TimeFormat, TimeOutputFormat } from '../../../helpers/functions/time/ParseTime'

@injectable()
export class BattleFieldsQueuePageParser {
  public async getRemainingTime(page: Page): Promise<number> {
    const timeElement = await page.$('.cntr span.notify')
    if (!timeElement) {
      return 0
    }

    const timeText = await timeElement.evaluate((el) => el.textContent)
    if (!timeText) {
      return 0
    }

    return parseTime(timeText, TimeFormat.SIMPLE, TimeOutputFormat.RELATIVE_MILLISECONDS)
  }
}
