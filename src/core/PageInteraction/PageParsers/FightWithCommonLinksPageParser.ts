import { injectable } from 'inversify'
import { Page } from 'puppeteer'

@injectable()
export class FightWithCommonLinksPageParser {
  public async getFightersCount(page: Page): Promise<{ warriors: number; healers: number }> {
    return page.evaluate(() => {
      const parentElement = document.querySelector('img[src="/images/icons/red_warrior.png"]')?.parentElement
      if (!parentElement) {
        return {
          warriors: 0,
          healers: 0,
        }
      }

      return {
        warriors: parseInt(parentElement.children[5].textContent || '0'),
        healers: parseInt(parentElement.children[7].textContent || '0'),
      }
    })
  }
}
