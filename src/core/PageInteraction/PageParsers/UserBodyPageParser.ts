import { injectable } from 'inversify'
import { Page } from 'puppeteer'
import { Link, PageParser } from '../PageParser'

export type UserBodyPageData = { cost: number; link: Link }[]

@injectable()
export class UserBodyPageParser {
  public async getPageData(page: Page): Promise<UserBodyPageData> {
    const repairLinks = await PageParser.getLinks(page, 'a[href*="repairLink"]')

    const result: UserBodyPageData = []

    for (const linkText in repairLinks) {
      let repairCostText = linkText.match(/\d+/)
      if (!repairCostText) {
        throw new Error(`Cant parse link text "${linkText}"`)
      }
      const repairCost = parseInt(repairCostText[0])
      result.push({
        cost: repairCost,
        link: repairLinks[linkText],
      })
    }

    return result
  }
}
