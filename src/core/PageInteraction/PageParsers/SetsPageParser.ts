import { inject, injectable } from 'inversify'
import pino from 'pino'
import { Page } from 'puppeteer'
import { ContainerTypes } from '../../../helpers/ContainerValues'
import Logger = pino.Logger

export enum SetNames {
  TOWERS = 'Башня',
  FIGHT = 'Сражения',
}

export enum SpellNames {
  BERSERK = 'Бepcepк',
  CRIT = 'Критoмания',
  ROCK_SHIELD = 'Кaмeнный щит',
  ENERGY_SHIELD = 'Энергетический щит',
  DODGE = 'Увoрот',
}

export type DesiredSetsConfig = { [key in SetNames]: DesiredSpells }
export type DesiredSpells = SpellNames[][]
export type EditorPageSpellData = { name: SpellNames; makeFirstHref: string | null }

export type EditorPageSpellSet = {
  chosenSpells: EditorPageSpellData[]
  availableSpells: EditorPageSpellData[]
  name: string | null
}

@injectable()
export class SetsPageParser {
  constructor(
    @inject(ContainerTypes.LOGGER)
    private readonly logger: Logger,
  ) {}

  public async parseSetsEditorPage(page: Page): Promise<EditorPageSpellSet> {
    // @ts-ignore
    const chosenSpells = await this.getSpellsData(page, SpellState.CHOSEN)
    if (chosenSpells.length !== 3) {
      throw new Error('Cant parse chosen spells in spells sets editor page')
    }

    const availableSpells = await this.getSpellsData(page, SpellState.AVAILABLE)

    let setName = null
    try {
      setName = (await page.$eval('select[name="setChoose"] option[selected]', (el) => el?.textContent?.trim())) || null
    } catch (e) {}

    return {
      chosenSpells,
      availableSpells,
      name: setName,
    }
  }

  public async getPageError(page: Page): Promise<string | null> {
    const error = await page.$('span.feedbackPanelERROR')
    if (!error) {
      return null
    }

    return await error.evaluate((el) => el.textContent)
  }

  private async getSpellsData(
    page: Page,
    spellState: SpellState,
  ): Promise<
    {
      makeFirstHref: string
      name: SpellNames
    }[]
  > {
    await page.waitForNetworkIdle()
    const chosenSpellsPageData = await page.evaluate((spellState) => {
      let spellElements = []
      if (spellState === 'Выбранные') {
        spellElements = Array.from(
          document.evaluate(
            `//b[contains(text(),'${spellState}:')]/..`,
            document,
            null,
            XPathResult.FIRST_ORDERED_NODE_TYPE,
            null,
            // @ts-ignore
          ).singleNodeValue?.children || [],
        ).slice(1)
      } else if (spellState === 'Доступные') {
        let firstElement: any = document.evaluate(
          `//b[contains(text(),'${spellState}:')]`,
          document,
          null,
          XPathResult.FIRST_ORDERED_NODE_TYPE,
          null,
          // @ts-ignore
        ).singleNodeValue
        firstElement = firstElement?.nextElementSibling
        while (firstElement && !firstElement.classList.contains('hr')) {
          spellElements.push(firstElement)
          firstElement = firstElement?.nextElementSibling
        }
      } else {
        return []
      }

      return spellElements
        .filter((row) => !!row.children[0]?.children[1]?.textContent)
        .map((row) => ({
          // @ts-ignore
          makeFirstHref: row.children[1] ? row.children[1].getAttribute('href') : null,
          // @ts-ignore
          text: row.children[0].children[1].textContent,
        }))
    }, spellState)

    return chosenSpellsPageData.map((chosenSpell) => {
      const name = Object.values(SpellNames).find((name) => chosenSpell.text === name)
      if (!name) {
        throw new Error(`No such spell in spell names enum "${chosenSpell.text}"`)
      }
      return {
        makeFirstHref: chosenSpell.makeFirstHref,
        name,
      }
    })
  }
}

export enum SpellState {
  CHOSEN = 'Выбранные',
  AVAILABLE = 'Доступные',
}
