import { inject, injectable } from 'inversify'
import pino from 'pino'
import { Page } from 'puppeteer'
import { ContainerTypes } from '../../../helpers/ContainerValues'
import { Link } from '../PageParser'
import Logger = pino.Logger

export enum TalentName {
  BURNING_ANGER = 'Выжигающий гнев',
  HARDENING = 'Закалка',
  ATLAS_TREE = 'Мастерство Атланта',
  SEARING_ENERGY_SHIELD = 'Жгучий энергощит',
  ACCELERATED_RYTHM = 'Ускоренный ритм',
  CRIT_RELEASE = 'Стихия крита',
  EVASION = 'Уклонение',
  ATLAS_AGILITY = 'Ловкость Атланта',
  HEALING_POTIONS = 'Заживляющие зелья',
  FAME = 'Известность',
  RELIABLE_ARMOUR = 'Надежная броня',
}

export type TalentPriorities = Map<TalentName, number | TalentPriorities>
export type Talent = {
  invested: number
  max: number
  href: string
}

@injectable()
export class TalentPageParser {
  constructor(
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
  ) {}

  public async getAvailableTalentPoints(page: Page): Promise<number> {
    const talentPointsBlockText = await page.evaluate(() => {
      return document.querySelector('img[src="/images/icons/bluestar.png"]')?.parentElement?.textContent
    })

    if (typeof talentPointsBlockText !== 'string') {
      throw new Error('Cant determine available talent points')
    }

    const talentPointsBlockNumber = talentPointsBlockText.match(/\d+/g)
    if (!talentPointsBlockNumber) {
      throw new Error('Cant determine available talent points')
    }

    return parseInt(talentPointsBlockNumber[0])
  }

  public async getTalentInfoByLink(page: Page, link: Link): Promise<Talent> {
    return page.evaluate((linkHref) => {
      const distributedText =
        document.querySelector(`a[href="${linkHref}"]`)?.parentElement?.children[2]?.textContent || '[0/10]'
      const matches = distributedText.match(/\[(\d+)\/(\d+)]/)
      if (!matches) {
        throw new Error('Cannot parse talents page in HandleStuffOnHeroPageMiddleware')
      }
      return {
        invested: parseInt(matches[1]),
        max: parseInt(matches[2]),
        href: linkHref,
      }
    }, link.href)
  }
}
