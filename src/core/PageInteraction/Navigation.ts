import { HTTPResponse, Page } from 'puppeteer'
import { container } from '../../container'
import { AccountProvider, IEnvironment } from '../../helpers/environment'
import { ContainerTypes } from '../../helpers/ContainerValues'

export async function followLink(page: Page, path: string): Promise<HTTPResponse | null> {
  const env = container.get<IEnvironment>(ContainerTypes.ENVIRONMENT)
  const domain = env.ACCOUNT_PROVIDER === AccountProvider.PLAIN ? 'https://barbars.ru' : barbarianUrl('')
  const fullUrl = new URL(path, domain)

  const e = await Promise.all([
    page.goto(fullUrl.toString(), {
      waitUntil: 'networkidle0',
      timeout: 60_000,
    }),
    page.waitForNavigation({
      timeout: 60_000,
    }),
  ])

  return e[0]
}

export function barbarianUrl(path: string): string {
  if (path[0] !== '/') {
    path = '/' + path
  }

  return 'http://br.spaces-games.com' + path
}

export function spacesUrl(path: string): string {
  if (path[0] !== '/') {
    path = '/' + path
  }

  return `https://spaces.im` + path
}
