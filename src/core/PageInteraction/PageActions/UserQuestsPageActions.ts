import { injectable } from 'inversify'
import { Page } from 'puppeteer'
import { LinkName } from '../../../helpers/constants/LinkName'
import { followLink } from '../Navigation'
import { PageParser } from '../PageParser'

@injectable()
export class UserQuestsPageActions {
  public async collectAllRewards(page: Page): Promise<void> {
    await followLink(page, '/user/quests')
    while (await PageParser.followLinkIfExists(LinkName.COLLECT_REWARD, page, 'a')) {}
  }
}
