import { inject, injectable } from 'inversify'
import pino from 'pino'
import { LinkWithName } from '../../../handlers/HandlerContract'
import { LinkName } from '../../../helpers/constants/LinkName'
import { ContainerTypes } from '../../../helpers/ContainerValues'
import delay from '../../../helpers/functions/delay'
import { LiveStats } from '../../../helpers/LiveStats'
import { Storage } from '../../../helpers/Storage'
import { FighterBotCarry } from '../../bots/FighterBot'
import { followLink } from '../Navigation'
import { PageParser } from '../PageParser'
import { FightWithCommonLinksPageParser } from '../PageParsers/FightWithCommonLinksPageParser'
import Logger = pino.Logger

export enum AdvanceLocationsAlgorithm {
  NONE,
  SIMPLE,
  FOLLOW_ALLIES,
}

export enum FighterMode {
  FIGHTER,
  HEALER,
  ENERGY_BURNER,
}

export enum WaitAfterAttackMode {
  NONE,
  ENERGY_EFFICIENT,
  PVP,
  ENERGY_EFFICIENT_WITH_GLOBAL,
  PVP_WITH_GLOBAL,
}

export type FightSettings = {
  advanceLocationsAlg: AdvanceLocationsAlgorithm
  fighterMode: FighterMode
  shouldDrinkPotions: boolean
  waitAfterAttackMode: WaitAfterAttackMode
}

@injectable()
export class FightWithCommonLinksPageActions {
  constructor(
    @inject(ContainerTypes.LOGGER)
    private readonly logger: Logger,
    private readonly liveStats: LiveStats,
    private readonly storage: Storage,
    private readonly pageParser: FightWithCommonLinksPageParser,
  ) {}

  public async handleFight(carry: FighterBotCarry, settings: FightSettings): Promise<void> {
    this.logger.info('Fight started')
    const fightStartTime = Date.now()

    while (true) {
      const shouldContinue = await this.fightTick(carry, settings)
      if (!shouldContinue) {
        break
      }
    }

    this.logger.info('Fight ended in %s seconds', Math.round((Date.now() - fightStartTime) / 1000))
  }

  public async advanceLocations(carry: FighterBotCarry, algorithm: AdvanceLocationsAlgorithm): Promise<boolean> {
    const config: { [key in AdvanceLocationsAlgorithm]: (carry: FighterBotCarry) => Promise<boolean> } = {
      [AdvanceLocationsAlgorithm.NONE]: async () => false,
      [AdvanceLocationsAlgorithm.SIMPLE]: this.advanceTowerSimpleAlg.bind(this),
      [AdvanceLocationsAlgorithm.FOLLOW_ALLIES]: this.advanceTowersFollowAlliesAlg.bind(this),
    }

    return await config[algorithm](carry)
  }

  public async fightTick(carry: FighterBotCarry, settings: FightSettings): Promise<boolean> {
    try {
      const chosenLink = await this.selectAction(carry, settings)
      if (!chosenLink) {
        return false
      }

      await this.preAttackActions(carry, settings)

      await followLink(carry.page, chosenLink.href)

      await this.postAttackActions(carry, chosenLink, settings)
    } catch (e) {}

    return true
  }

  private async preAttackActions(carry: FighterBotCarry, settings: FightSettings) {
    await this.advanceLocations(carry, settings.advanceLocationsAlg)
  }

  private async advanceTowersFollowAlliesAlg(): Promise<boolean> {
    return false
  }

  private async advanceTowerSimpleAlg(carry: FighterBotCarry): Promise<boolean> {
    const { warriors, healers } = await this.pageParser.getFightersCount(carry.page)

    if (warriors + healers >= 3) {
      return false
    }

    const towerImagesCount = await carry.page.evaluate(
      () => Array.from(document.querySelectorAll('img[src="/images/icons/red_tower.png"]')).length,
    )
    if (towerImagesCount === 2) {
      return false
    }

    const forwardLinks = await PageParser.getLinks(
      carry.page,
      'a.flhdr:not(.minor) img[src="/images/icons/bluearrow-s.png"], a.flhdr:not(.minor) img[src="/images/icons/redarrow-s.png"]',
      { useParent: true },
    )

    if (!Object.keys(forwardLinks).length) {
      this.logger.info('Looks like there is no forward path. Going to HUB')
      await followLink(carry.page, '/game/towers')
      return false
    }

    const bestKey = Object.keys(forwardLinks).slice(-1)[0]
    this.logger.info('Heading to "%s" because of low amount of enemies', bestKey)
    await followLink(carry.page, forwardLinks[bestKey].href)
    return true
  }

  private async selectAction(carry: FighterBotCarry, settings: FightSettings): Promise<LinkWithName | null> {
    const links = await PageParser.getLinks(carry.page, 'a')
    const fightPriorities: { [key in FighterMode]: LinkName[] } = {
      [FighterMode.ENERGY_BURNER]: [LinkName.BURN_ENERGY, LinkName.ATTACK_ENEMIES, LinkName.HEAL_ALLIES],
      [FighterMode.HEALER]: [LinkName.HEAL_ALLIES, LinkName.BURN_ENERGY, LinkName.ATTACK_ENEMIES],
      [FighterMode.FIGHTER]: [LinkName.ATTACK_ENEMIES, LinkName.BURN_ENERGY, LinkName.HEAL_ALLIES],
    }

    const chosenLinkName = fightPriorities[settings.fighterMode].find((linkName) => !!links[linkName])
    if (!chosenLinkName) {
      return null
    }
    const chosenLink = links[chosenLinkName]

    const abilityLinksNames = Object.keys(links).filter((link) => link.includes('(гoтoво)'))
    if (abilityLinksNames.length) {
      return links[abilityLinksNames[abilityLinksNames.length - 1]]
    }

    if (chosenLink.name === LinkName.ATTACK_ENEMIES) {
      const finishOffLink = Object.values(links).find((o) => o.name.startsWith(LinkName.FINISH_OFF))
      if (finishOffLink) {
        return finishOffLink
      }
    }

    return chosenLink
  }

  private async postAttackActions(carry: FighterBotCarry, chosenLink: LinkWithName, settings: FightSettings) {
    await carry.hero.updateGlobalInfo(carry.page)

    if (settings.shouldDrinkPotions) {
      await this.drinkPotion(carry)
    }

    await this.healFriendIfCantHealAnybody(chosenLink, carry)

    await this.liveStats.logDamage(carry.page)

    if (settings.waitAfterAttackMode === WaitAfterAttackMode.PVP_WITH_GLOBAL) {
      await delay(this.storage.actionDelayWithGlobalInPvP)
    } else if (settings.waitAfterAttackMode === WaitAfterAttackMode.PVP) {
      await delay(this.storage.actionDelayInPVP)
    } else if (settings.waitAfterAttackMode === WaitAfterAttackMode.ENERGY_EFFICIENT_WITH_GLOBAL) {
      await delay(this.storage.actionDelayWithGlobal)
    } else if (settings.waitAfterAttackMode === WaitAfterAttackMode.ENERGY_EFFICIENT) {
      await delay(this.storage.actionDelay)
    } else if (settings.waitAfterAttackMode !== WaitAfterAttackMode.NONE) {
      throw new Error(`Unhandled wait after attack mode`)
    }
  }

  private async healFriendIfCantHealAnybody(chosenLink: LinkWithName, carry: FighterBotCarry) {
    let links = await PageParser.getLinks(carry.page, 'a')
    if (chosenLink.name !== LinkName.HEAL_ALLIES) {
      return
    }

    const cantHealAnybody = await PageParser.getLastLogMessageIfContains(carry.page, 'Лечить некого')
    if (cantHealAnybody !== null) {
      const healTargetField = Object.values(links)
        .reverse()
        .find((o) => o.name.startsWith(LinkName.HEAL))
      if (healTargetField && healTargetField.name !== LinkName.HEAL_ALLIES) {
        await followLink(carry.page, healTargetField.href)
      }
    }
  }

  private async drinkPotion(carry: FighterBotCarry) {
    const links = await PageParser.getLinks(carry.page, 'a')
    const potionLinkButton = Object.values(links).find((o) => o.name.startsWith(LinkName.DRINK_POTION))
    if ((await carry.hero.shouldUsePotion()) && potionLinkButton) {
      await followLink(carry.page, potionLinkButton.href)
    }
  }
}
