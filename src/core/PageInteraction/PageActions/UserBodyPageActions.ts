import { Page } from 'puppeteer'
import { provideSingleton } from '../../../helpers/functions/provideSingleton'
import { Storage } from '../../../helpers/Storage'
import { Hero } from '../../Hero'
import { followLink } from '../Navigation'
import { UserBodyPageParser } from '../PageParsers/UserBodyPageParser'

export interface UserBodyPageActionsData {
  needIron: number
  latestCantRepairTime: number
}

@provideSingleton(UserBodyPageActions)
export class UserBodyPageActions {
  protected needIron: number = 0
  protected latestCantRepairTime: number = 0

  constructor(
    protected readonly userBodyPageParser: UserBodyPageParser,
    protected readonly storage: Storage,
  ) {
    this.restoreFromStorage()
  }

  protected restoreFromStorage() {
    if (!this.storage.userBodyPageActionsData) {
      return
    }

    this.needIron = this.storage.userBodyPageActionsData.needIron
    this.latestCantRepairTime = this.storage.userBodyPageActionsData.latestCantRepairTime
  }

  protected writeToStorage() {
    this.storage.userBodyPageActionsData = {
      needIron: this.needIron,
      latestCantRepairTime: this.latestCantRepairTime,
    }
  }

  public getNeededIronAmount(): number {
    return this.needIron
  }

  public getLatestUnsuccessfulRepairTime(): number {
    return this.latestCantRepairTime
  }

  public async repairGear(page: Page, hero: Hero): Promise<void> {
    if (hero.iron < this.needIron) {
      return
    }

    await followLink(page, '/user/body')

    let pageData
    while ((pageData = await this.userBodyPageParser.getPageData(page)) && pageData.length) {
      const repairWithLessCost = pageData.reduce((acc: any, repair: any) => {
        return acc.cost < repair.cost ? acc : repair
      }, pageData[0])

      if (repairWithLessCost.cost > hero.iron && pageData.length > 1) {
        this.needIron = repairWithLessCost.cost
        this.latestCantRepairTime = Date.now()
        this.writeToStorage()
        return
      }

      await followLink(page, repairWithLessCost.link.href)
      hero.iron -= repairWithLessCost.cost
    }

    this.needIron = 0
    this.latestCantRepairTime = 0
    this.writeToStorage()
  }

  public async hasBrokenGear(page: Page): Promise<boolean> {
    return !!(await page.$('img[src="/images/icons/clothes_broken.gif"]'))
  }
}
