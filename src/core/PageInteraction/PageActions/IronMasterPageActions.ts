import { Page } from 'puppeteer'
import { provideSingleton } from '../../../helpers/functions/provideSingleton'
import { Hero } from '../../Hero'
import { followLink } from '../Navigation'
import { IronMasterPageData, IronMasterPageParser } from '../PageParsers/IronMasterPageParser'

@provideSingleton(IronMasterPageActions)
export class IronMasterPageActions {
  constructor(private readonly pageParser: IronMasterPageParser) {}

  public async gatherExp(page: Page, hero: Hero): Promise<boolean> {
    await followLink(page, '/trade/ironmaster')
    const pageData: IronMasterPageData = await this.pageParser.parsePage(page)
    if (!pageData.donationLinks.iron.length) {
      return false
    }

    const ironDonationLinks = pageData.donationLinks.iron.sort((a, b) => b.amount - a.amount)
    let bestLink = null
    for (let i = 0; i < ironDonationLinks.length; i++) {
      const link = ironDonationLinks[i]
      if (hero.iron < link.amount) {
        continue
      }

      bestLink = link
      break
    }

    if (!bestLink) {
      return false
    }

    await followLink(page, bestLink.link.href)
    return true
  }
}
