import { injectable } from 'inversify'
import { Page } from 'puppeteer'
import { LinkName } from '../../../helpers/constants/LinkName'
import { HeroConfig } from '../../../helpers/HeroConfig'
import { followLink } from '../Navigation'
import { PageParser } from '../PageParser'
import { TalentPageParser, TalentPriorities } from '../PageParsers/TalentPageParser'

@injectable()
export class TalentPageActions {
  constructor(private readonly talentPageParser: TalentPageParser) {}

  public async spendTalentPoints(page: Page) {
    await followLink(page, '/user/stances')

    const priorities = await HeroConfig.getTalentUpgradePriorities()

    let talentPoints = await this.talentPageParser.getAvailableTalentPoints(page)
    while (talentPoints) {
      await this.handleTalentMapOnCurrentPage(page, priorities)

      await followLink(page, '/user/stances')

      const newTalentPoints = await this.talentPageParser.getAvailableTalentPoints(page)
      if (newTalentPoints === talentPoints) {
        throw new Error('Cant spend talent point')
      }

      talentPoints = newTalentPoints
    }
  }

  protected async handleTalentMapOnCurrentPage(page: Page, priorities: TalentPriorities) {
    for (const [talentName, talentPointsOrPriority] of priorities.entries()) {
      const links = await PageParser.getLinks(page, 'a[href*="user/talent"]')
      if (!links[talentName]) {
        throw new Error(`No such talent "${talentName}"`)
      }

      if (typeof talentPointsOrPriority !== 'number') {
        await followLink(page, links[talentName].href)
        await this.handleTalentMapOnCurrentPage(page, talentPointsOrPriority)
        await page.goBack()
        continue
      }

      const talent = await this.talentPageParser.getTalentInfoByLink(page, links[talentName])

      if (talent.invested >= talentPointsOrPriority) {
        continue
      }

      await followLink(page, links[talentName].href)
      const foundLink = await PageParser.followLinkIfExists(LinkName.LEARN_FOR, page, 'a[href*="runkUpLink"]')
      if (!foundLink) {
        throw new Error('Cant find link to learn skill')
      }

      return
    }
  }
}
