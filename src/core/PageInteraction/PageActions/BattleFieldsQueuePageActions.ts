import { Page } from 'puppeteer'
import { provideSingleton } from '../../../helpers/functions/provideSingleton'
import { Storage } from '../../../helpers/Storage'
import { followLink } from '../Navigation'
import { BattleFieldsQueuePageParser } from '../PageParsers/BattleFieldsQueuePageParser'

@provideSingleton(BattleFieldsQueuePageActions)
export class BattleFieldsQueuePageActions {
  constructor(private readonly pageParser: BattleFieldsQueuePageParser) {}
  public async getRemainingTime(page: Page): Promise<number> {
    await followLink(page, '/game/bg')
    return await this.pageParser.getRemainingTime(page)
  }
}
