import { inject } from 'inversify'
import pino from 'pino'
import { Page } from 'puppeteer'
import { LinkName } from '../../../helpers/constants/LinkName'
import { ContainerTypes } from '../../../helpers/ContainerValues'
import { provideSingleton } from '../../../helpers/functions/provideSingleton'
import { Hero } from '../../Hero'
import { followLink } from '../Navigation'
import { PageParser } from '../PageParser'
import {
  DesiredSetsConfig,
  DesiredSpells,
  EditorPageSpellSet,
  SetNames,
  SetsPageParser,
  SpellNames,
} from '../PageParsers/SetsPageParser'
import Logger = pino.Logger

@provideSingleton(SetsPageActions)
export class SetsPageActions {
  public currentSet: SetNames | null = null
  private readonly availableSets: { [key in SetNames]: string | null } = {
    [SetNames.FIGHT]: null,
    [SetNames.TOWERS]: null,
  }
  private readonly desiredSetsConfig: DesiredSetsConfig = {
    [SetNames.FIGHT]: [[SpellNames.ENERGY_SHIELD, SpellNames.ROCK_SHIELD], [SpellNames.CRIT], [SpellNames.BERSERK]],
    [SetNames.TOWERS]: [[SpellNames.BERSERK]],
  }

  constructor(
    private setsPageParser: SetsPageParser,
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
  ) {}

  public async upgradeSetsIfNeeded(page: Page, hero: Hero): Promise<void> {
    // we do not have sets and this is disappointing
    if (hero.level < 30) {
      return
    }

    await this.goToSetsPage(page)
    const select = await page.$('select[name="setChoose"]')
    if (!select || (await select.evaluate((s) => s.childElementCount < 2))) {
      for (let i = 0; i < 2; i++) {
        await PageParser.bgSafeClick(page, `a[href*="saveSets:${i}"]`)
      }
    }

    let setIndex = 0
    for (const setName in this.desiredSetsConfig) {
      await page.select('select[name="setChoose"]', String(setIndex + 1))
      await page.waitForNetworkIdle()

      let currentSet = await this.setsPageParser.parseSetsEditorPage(page)
      if (currentSet.name !== setName) {
        await page.type('input[name="abilitySetName"]', setName)
        await PageParser.bgSafeClick(page, `input[name=":submit"]`)
        currentSet = await this.setsPageParser.parseSetsEditorPage(page)
      }

      const desiredSpells = this.desiredSetsConfig[setName as SetNames]
      const isChanged = await this.upgradeCurrentSpellSet(currentSet, desiredSpells, page)
      if (isChanged) {
        await PageParser.bgSafeClick(page, `a[href*="setsPanel1:saveSets:${setIndex}:saveSetLink"]`)
      }

      setIndex++
      this.availableSets[setName as SetNames] = String(setIndex)
    }
  }

  private async goToSetsPage(page: Page) {
    await followLink(page, '/')
    await followLink(page, '/user/sets')
    await PageParser.followLinkIfExists(LinkName.MY_ABILITIES, page, 'a')
  }

  public async chooseSet(page: Page, desiredSet: SetNames): Promise<void> {
    if (this.currentSet === desiredSet) {
      return
    }

    if (this.availableSets[desiredSet] === null) {
      // TODO: choose set based on spell config
      this.logger.info('We dont have spell sets, ignoring it for now...')
      this.currentSet = desiredSet
      return
    }

    await followLink(page, '/user/sets')

    this.logger.info('Switching set to %s', desiredSet)
    await page.select('select[name="setChoose"]', this.availableSets[desiredSet] as string)
    await page.waitForNetworkIdle()

    const error = await this.setsPageParser.getPageError(page)
    if (error) {
      if (error !== 'Менять умения в бою нельзя') {
        throw new Error('Unknown error occurred while choosing set')
      }

      const links = await PageParser.getLinks(page)
      if (!links[LinkName.LEAVE_FIGHT]) {
        throw new Error('Cant handle error when choosing set ')
      }

      this.logger.info('Looks i am in fight. Leaving it and lets try again')
      await followLink(page, links[LinkName.LEAVE_FIGHT].href)
      await this.chooseSet(page, desiredSet)
    }

    this.currentSet = desiredSet
  }

  private async upgradeCurrentSpellSet(
    currentSet: EditorPageSpellSet,
    desiredSpells: SpellNames[][],
    page: Page,
  ): Promise<boolean> {
    let setChanged = false
    if (!(await this.isSpellsWhereExpected(currentSet, desiredSpells))) {
      await this.changeSpellsTo(page, currentSet, desiredSpells)
      setChanged = true
    }

    return setChanged
  }

  private async changeSpellsTo(
    page: Page,
    actualSpells: EditorPageSpellSet,
    desiredSpells: DesiredSpells,
  ): Promise<void> {
    for (let slotN = desiredSpells.length - 1; slotN >= 0; slotN--) {
      const desiredSpellsInCurrentSlot = desiredSpells[slotN]

      const chosenSpell = actualSpells.chosenSpells[0]
      const availableSpells = actualSpells.availableSpells

      for (let i = 0; i < desiredSpellsInCurrentSlot.length; i++) {
        if (chosenSpell.name === desiredSpellsInCurrentSlot[i]) {
          break
        }

        const spellToPushTop =
          availableSpells.find((availableSpell) => availableSpell.name === desiredSpellsInCurrentSlot[i]) ||
          actualSpells.chosenSpells.find((spell) => spell.name === desiredSpellsInCurrentSlot[i])

        if (spellToPushTop && spellToPushTop.makeFirstHref) {
          await followLink(page, spellToPushTop.makeFirstHref)
          actualSpells = await this.setsPageParser.parseSetsEditorPage(page)
          break
        }
      }
    }
  }

  private async isSpellsWhereExpected(
    actualSetState: EditorPageSpellSet,
    desiredSpells: DesiredSpells,
  ): Promise<boolean> {
    for (let slotN = 0; slotN < desiredSpells.length; slotN++) {
      const desiredSpellsInCurrentSlot = desiredSpells[slotN]

      const chosenSpell = actualSetState.chosenSpells[slotN]
      const availableSpells = actualSetState.availableSpells
      for (let i = 0; i < desiredSpellsInCurrentSlot.length; i++) {
        if (chosenSpell.name === desiredSpellsInCurrentSlot[i]) {
          break
        }

        // spell can be found in available spells
        if (availableSpells.find((availableSpell) => availableSpell.name === desiredSpellsInCurrentSlot[i])) {
          return false
        }

        // spell can be found in other slot
        if (actualSetState.chosenSpells.find((spell) => spell.name === desiredSpellsInCurrentSlot[i])) {
          return false
        }

        if (i !== 0) {
          throw new Error(`Cannot find spell "${desiredSpellsInCurrentSlot[i]}"`)
        }
      }
    }

    return true
  }
}
