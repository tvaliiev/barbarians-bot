import { Page } from 'puppeteer'
import { provideSingleton } from '../../../helpers/functions/provideSingleton'
import { parseTime, TimeFormat, TimeOutputFormat } from '../../../helpers/functions/time/ParseTime'
import { Hero } from '../../Hero'
import { followLink } from '../Navigation'
import {
  ClanQuest,
  ClanQuestsPageData,
  ClanQuestsPageParser,
  Quest,
  QuestMap,
} from '../PageParsers/ClanQuestsPageParser'

const questPriorities: ClanQuest[] = [ClanQuest.SURVIVAL]

@provideSingleton(ClanQuestsPageActions)
export class ClanQuestsPageActions {
  private nextBestQuestStartsAt: number = 0
  private preventQuest: { [key in ClanQuest]?: number } = {}

  constructor(private readonly pageParser: ClanQuestsPageParser) {}

  public doNotTakeQuest(quest: ClanQuest, till: number): void {
    this.preventQuest[quest] = till
  }

  public async initHeroQuestData(hero: Hero, page: Page) {
    if (!hero.inGuild) {
      return
    }

    const pageData = await this.pageParser.parsePage(page)
    this.updateHeroQuestFromPageData(hero, pageData)
  }

  public async dismissQuest(quest: ClanQuest, page: Page, hero: Hero): Promise<void> {
    if (!hero.inGuild || !hero.activeQuest || hero.activeQuest.name !== quest) {
      return
    }

    const pageData = await this.pageParser.parsePage(page)
    this.updateHeroQuestFromPageData(hero, pageData)
    if (pageData.activeQuest !== quest) {
      return
    }

    const activeQuest = pageData.availableQuests[quest]
    if (!activeQuest) {
      throw new Error('Cant dismiss quest: ' + quest)
    }

    if (!activeQuest.links.end) {
      throw new Error('No end link for active quest ' + activeQuest.name)
    }

    await followLink(page, activeQuest.links.end)
    this.doNotTakeQuest(quest, parseTime('1h', undefined, TimeOutputFormat.RELATIVE_MILLISECONDS))
    this.updateHeroQuest(hero, null)
  }

  public async updateQuestProgress(page: Page, hero: Hero): Promise<void> {
    if (!hero.inGuild) {
      return
    }

    const pageData = await this.pageParser.parsePage(page)
    this.updateHeroQuestFromPageData(hero, pageData, false)
  }

  public async takeBestQuest(page: Page, hero: Hero): Promise<void> {
    if (!hero.inGuild) {
      return
    }

    if (this.nextBestQuestStartsAt && this.nextBestQuestStartsAt >= Date.now()) {
      return
    }

    const pageData = await this.pageParser.parsePage(page)
    this.updateHeroQuestFromPageData(hero, pageData)
    if (pageData.activeQuest) {
      return
    }

    const bestQuest = this.getBestQuest(pageData.availableQuests)
    if (!bestQuest) {
      this.nextBestQuestStartsAt = parseTime('10m', TimeFormat.HUMAN_READABLE, TimeOutputFormat.RELATIVE_MILLISECONDS)
      return
    }

    await followLink(page, bestQuest.links.start as string)
    this.updateHeroQuest(hero, bestQuest)
  }

  protected getBestQuest(map: QuestMap): Quest | null {
    for (let i = 0; i < questPriorities.length; i++) {
      const quest: ClanQuest = questPriorities[i]
      const questData: Quest | undefined = map[quest]
      if (!questData) {
        throw new Error('No data for quest ' + quest)
      }

      if (!questData.isAvailable) {
        continue
      }

      if (Date.now() < (this.preventQuest[quest] || 0)) {
        continue
      }

      return questData
    }

    return null
  }

  private updateHeroQuestFromPageData(
    hero: Hero,
    pageData: ClanQuestsPageData,
    updateNextBestQuestStart: boolean = true,
  ) {
    this.updateHeroQuest(
      hero,
      pageData.activeQuest ? pageData.availableQuests[pageData.activeQuest] : null,
      updateNextBestQuestStart,
    )
  }

  private updateHeroQuest(hero: Hero, quest: Quest | null | undefined, updateNextQuestStart: boolean = true) {
    if (!quest) {
      hero.activeQuest = null
      if (updateNextQuestStart) {
        this.nextBestQuestStartsAt = 0
      }
      return
    }

    hero.activeQuest = quest
    if (updateNextQuestStart) {
      this.nextBestQuestStartsAt = quest.endsAt || 0
    }

    return
  }
}
