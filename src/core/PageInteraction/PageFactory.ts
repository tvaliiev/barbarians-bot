import { Browser, Page } from 'puppeteer'
import { followLink } from './Navigation'

export class PageFactory {
  public static async make(browser: Browser): Promise<Page> {
    const page = await browser.newPage()
    await page.setViewport({ width: 500, height: 950 })
    await followLink(page, '/user')
    return page
  }
}
