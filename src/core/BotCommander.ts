import { multiInject } from 'inversify'
import { provide } from 'inversify-binding-decorators'
import pino from 'pino'
import { container } from '../container'
import { ContainerTypes } from '../helpers/ContainerValues'
import { GenericBot } from './bots/GenericBot'
import Logger = pino.Logger

@provide(BotCommander)
export class BotCommander {
  constructor(
    @multiInject(ContainerTypes.BOTS)
    private bots: GenericBot[],
  ) {}

  public async run() {
    return Promise.all(this.bots.map(this.runBot))
  }

  public async runBot(bot: GenericBot): Promise<void> {
    await bot.run()
    container.get<Logger>(ContainerTypes.LOGGER).info(`Bot ${bot.constructor.name} exited`)
    await this.runBot(bot)
  }

  public stop() {
    this.bots.forEach((bot) => bot.stop())
  }
}
