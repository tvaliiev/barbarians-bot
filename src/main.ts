import '@abraham/reflection'
import { buildProviderModule } from 'inversify-binding-decorators'
import pino from 'pino'
import { Browser } from 'puppeteer'
import { container } from './container'
import { BotCommander } from './core/BotCommander'
import { ContainerTypes } from './helpers/ContainerValues'
import Logger = pino.Logger

container.load(buildProviderModule())
;(async () => {
  const logger = container.get<Logger>(ContainerTypes.LOGGER)
  logger.info('Bot is initializing...')
  const botCommander = await container.getAsync(BotCommander)

  const browser = await container.getAsync<Browser>(ContainerTypes.BROWSER)
  process.on('SIGINT', async () => {
    botCommander.stop()
    await browser.close()
  })

  logger.info('Bot is initialized')
  try {
    await botCommander.run()
  } catch (e) {
    logger.error(e)
  }

  await browser.close()
})()
