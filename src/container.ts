import { classesContainer } from './containers/classes'

classesContainer.options.autoBindInjectable = true
classesContainer.options.defaultScope = 'Singleton'

export const container = classesContainer
