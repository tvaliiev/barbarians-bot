import { inject } from 'inversify'
import { provide } from 'inversify-binding-decorators'
import pino from 'pino'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { followLink } from '../../core/PageInteraction/Navigation'
import { PageParser } from '../../core/PageInteraction/PageParser'
import { CallbackMode } from '../../helpers/CallbackArray'
import { LinkName } from '../../helpers/constants/LinkName'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { NextPipeline, Pipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'
import Logger = pino.Logger

@provide(HandleFullBackpackMiddleware)
export class HandleFullBackpackMiddleware implements HandlerContract<FighterBotCarry> {
  private ignoreMiddleware = false

  constructor(
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
  ) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    const bagFullIcon = await carry.page.$('img[src="/images/icons/bag_full.gif"]')
    if (!bagFullIcon || this.ignoreMiddleware) {
      return next(carry)
    }

    this.logger.info('Looks like i have full backpack of items')

    return new Pipeline<FighterBotCarry>()
      .send(carry)
      .through(
        [this.tryToConvertAllToIron, this.tryIgnoreProblemIfNextLevelIsNear, this.failMiserably].map((o) =>
          o.bind(this),
        ),
      )
      .thenReturn()
  }

  protected async tryToConvertAllToIron(
    carry: FighterBotCarry,
    next: NextPipeline<FighterBotCarry>,
  ): Promise<FighterBotCarry> {
    await followLink(carry.page, '/user/rack')

    const links = await PageParser.getLinks(carry.page, 'a')
    if (!Object.hasOwn(links, LinkName.CONVERT_ALL_TO_IRON)) {
      this.logger.info('Cant handle backpack because of missing convert all to iron button')
      return next(carry)
    }

    await followLink(carry.page, links[LinkName.CONVERT_ALL_TO_IRON].href)
    const subLinks = await PageParser.getLinks(carry.page)
    if (!Object.hasOwn(subLinks, LinkName.YES_CONFIRM)) {
      throw new Error('while handling full backpack, clicked on convert all to iron but did not get confirmation page')
    }

    await followLink(carry.page, subLinks[LinkName.YES_CONFIRM].href)
    this.logger.info('Got rid of excess items in backpack by converting them to iron')

    return carry
  }

  protected async tryIgnoreProblemIfNextLevelIsNear(
    carry: FighterBotCarry,
    next: NextPipeline<FighterBotCarry>,
  ): Promise<FighterBotCarry> {
    await followLink(carry.page, '/user/rack')

    if (carry.hero.experience <= 80) {
      return next(carry)
    }

    this.logger.info('We can ignore full backpack because we soon will reach next level')

    this.ignoreMiddleware = true
    carry.hero.registerLevelUpCallback(
      HandleFullBackpackMiddleware.name + '.tryIgnoreProblemIfNextLevelIsNear',
      (hero, next) => {
        this.ignoreMiddleware = false
        return next(hero)
      },
      CallbackMode.ONCE,
    )

    return carry
  }

  protected async failMiserably(): Promise<FighterBotCarry> {
    throw new Error('Cannot handle full backpack =(')
  }
}
