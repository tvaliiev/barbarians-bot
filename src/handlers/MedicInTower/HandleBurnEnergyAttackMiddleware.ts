import { inject } from 'inversify'
import { provide } from 'inversify-binding-decorators'
import pino from 'pino'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { Hero } from '../../core/Hero'
import {
  AdvanceLocationsAlgorithm,
  FighterMode,
  FightWithCommonLinksPageActions,
  WaitAfterAttackMode,
} from '../../core/PageInteraction/PageActions/FightWithCommonLinksPageActions'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { NextPipeline } from '../../helpers/Pipeline'
import { Storage } from '../../helpers/Storage'
import { HandlerContract } from '../HandlerContract'
import Logger = pino.Logger

@provide(HandleBurnEnergyAttackMiddleware)
export class HandleBurnEnergyAttackMiddleware implements HandlerContract<FighterBotCarry> {
  constructor(
    private storage: Storage,
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
    private readonly pageActions: FightWithCommonLinksPageActions,
  ) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    if (Date.now() - this.storage.lastActionTime <= this.storage.actionDelay) {
      return next(carry)
    }
    this.storage.lastActionTime = Date.now()

    this.adjustHeroActionDelay(carry.hero)

    await this.pageActions.fightTick(carry, {
      advanceLocationsAlg: AdvanceLocationsAlgorithm.NONE,
      fighterMode: FighterMode.ENERGY_BURNER,
      shouldDrinkPotions: false,
      waitAfterAttackMode: WaitAfterAttackMode.NONE,
    })

    return carry
  }

  private adjustHeroActionDelay(hero: Hero) {
    const step = 10
    const bottomThreshold = hero.maxEnergy * 0.8
    const topThreshold = hero.maxEnergy * 0.9

    if (hero.energy < bottomThreshold) {
      this.storage.actionDelay += step
    } else if (hero.energy > topThreshold) {
      this.storage.actionDelay -= step
    }
  }
}
