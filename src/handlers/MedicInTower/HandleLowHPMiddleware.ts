import { inject } from 'inversify'
import { provide } from 'inversify-binding-decorators'
import pino from 'pino'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { followLink } from '../../core/PageInteraction/Navigation'
import { PageParser } from '../../core/PageInteraction/PageParser'
import { LinkName } from '../../helpers/constants/LinkName'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { NextPipeline } from '../../helpers/Pipeline'
import { Storage } from '../../helpers/Storage'
import { HandlerContract } from '../HandlerContract'
import Logger = pino.Logger

@provide(HandleLowHPMiddleware)
export class HandleLowHPMiddleware implements HandlerContract<FighterBotCarry> {
  constructor(
    private storage: Storage,
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
  ) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    const links = await PageParser.getLinks(carry.page)

    if (
      Object.hasOwn(links, LinkName.HEAL_MYSELF) &&
      Date.now() - this.storage.lastActionTime > this.storage.actionDelay
    ) {
      this.logger.info('I am hurt. Let me heal myself using "%s" button', LinkName.HEAL_MYSELF)
      this.storage.lastActionTime = Date.now()
      await followLink(carry.page, links[LinkName.HEAL_MYSELF].href)

      return carry
    }

    if (carry.hero.health === 0) {
      this.logger.info('Looks i am dead... Heading to home page')
      await followLink(carry.page, '/')
      return carry
    }

    return next(carry)
  }
}
