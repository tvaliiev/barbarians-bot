import { inject } from 'inversify'
import { provide } from 'inversify-binding-decorators'
import pino from 'pino'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { followLink } from '../../core/PageInteraction/Navigation'
import { PageParser } from '../../core/PageInteraction/PageParser'
import { LinkName } from '../../helpers/constants/LinkName'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'
import Logger = pino.Logger

@provide(HandlePutOnBetterThingPopupMiddleware)
export class HandlePutOnBetterThingPopupMiddleware implements HandlerContract<FighterBotCarry> {
  constructor(
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
  ) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    const links = await PageParser.getLinks(carry.page, 'a.info')

    if (Object.hasOwn(links, LinkName.PUT_ON)) {
      this.logger.info('Putting on new shiny item')
      await followLink(carry.page, links[LinkName.PUT_ON].href)
    }

    return next(carry)
  }
}
