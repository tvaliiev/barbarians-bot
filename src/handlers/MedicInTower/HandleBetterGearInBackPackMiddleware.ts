import { inject } from 'inversify'
import { provide } from 'inversify-binding-decorators'
import pino from 'pino'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { followLink } from '../../core/PageInteraction/Navigation'
import { PageParser } from '../../core/PageInteraction/PageParser'
import { LinkName } from '../../helpers/constants/LinkName'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'
import Logger = pino.Logger

@provide(HandleBetterGearInBackPackMiddleware)
export class HandleBetterGearInBackPackMiddleware implements HandlerContract<FighterBotCarry> {
  constructor(
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
  ) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    const link = await carry.page.$('img[src="/images/icons/bag_better.gif"]')
    if (link === null) {
      return next(carry)
    }

    await followLink(carry.page, '/user/rack')
    const betterGearLinks = await carry.page.evaluate(() => {
      return Array.from(document.querySelectorAll('table td[style="vertical-align:top"]'))
        .filter(
          (el) =>
            el.textContent &&
            el.textContent.match(/Лучше \(/) &&
            !el.children[2].children[0].children[0].classList.contains('itemBad'),
        )
        .map((current) => current.children[4].getAttribute('href'), {})
    })

    this.logger.info('Found %d better gear(-s) in backpack', betterGearLinks.length)

    for (let i = betterGearLinks.length - 1; i >= 0; i--) {
      const betterGearLink = betterGearLinks[i]
      if (!betterGearLink) {
        continue
      }

      await followLink(carry.page, betterGearLink)
      const links = await PageParser.getLinks(carry.page)
      if (links[LinkName.YES_CONFIRM]) {
        await followLink(carry.page, links[LinkName.YES_CONFIRM].href)
      }
    }

    return carry
  }
}
