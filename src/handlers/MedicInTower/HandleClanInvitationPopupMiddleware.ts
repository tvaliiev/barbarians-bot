import { inject } from 'inversify'
import { provide } from 'inversify-binding-decorators'
import pino from 'pino'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { followLink } from '../../core/PageInteraction/Navigation'
import { PageParser } from '../../core/PageInteraction/PageParser'
import { LinkName } from '../../helpers/constants/LinkName'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'
import Logger = pino.Logger

@provide(HandleClanInvitationPopupMiddleware)
export class HandleClanInvitationPopupMiddleware implements HandlerContract<FighterBotCarry> {
  constructor(
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
  ) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    const hasInvitation = await carry.page.evaluate(() => {
      const infoPanel = Array.from(document.querySelectorAll('span.info'))
      return infoPanel[0]?.children[1]?.textContent === 'приглашает тебя в клан'
    })

    if (hasInvitation) {
      const links = await PageParser.getLinks(carry.page, 'a[href*="statusPanel:inviteGuildPanel:declineLink"]')

      if (links[LinkName.DECLINE]) {
        this.logger.info('Declining clan invitaion...')
        await followLink(carry.page, links[LinkName.DECLINE].href)
      }
    }

    return next(carry)
  }
}
