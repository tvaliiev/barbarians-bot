import { inject } from 'inversify'
import { provide } from 'inversify-binding-decorators'
import pino from 'pino'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { followLink } from '../../core/PageInteraction/Navigation'
import { ContainerTypes } from '../../helpers/ContainerValues'
import delay from '../../helpers/functions/delay'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'
import Logger = pino.Logger

@provide(HandleLowEnergyMiddleware)
export class HandleLowEnergyMiddleware implements HandlerContract<FighterBotCarry> {
  constructor(
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
  ) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    const maxDamage = carry.hero.strength / 3
    if (carry.hero.energy < 50 || carry.hero.energy < (carry.hero.maxEnergy - maxDamage) * 0.35) {
      this.logger.info('Looks like i have low energy... Waiting 10 sec not in fight')

      await followLink(carry.page, '/')
      while (carry.hero.energy < carry.hero.maxEnergy * 0.9) {
        await delay(2000)
        await followLink(carry.page, '/')
        await carry.hero.updateGlobalInfo(carry.page)
      }
    }

    return next(carry)
  }
}
