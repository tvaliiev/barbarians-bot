import { NextPipeline } from '../helpers/Pipeline'

export type LinkObject = { [key: string]: LinkWithName }
export type LinkWithName = { href: string; name: string }

export interface HandlerContract<T> {
  handle(carry: T, next: NextPipeline<T>): Promise<T>
}
