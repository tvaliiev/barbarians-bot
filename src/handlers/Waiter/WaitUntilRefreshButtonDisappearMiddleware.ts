import pino from 'pino'
import { container } from '../../container'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { followLink } from '../../core/PageInteraction/Navigation'
import { PageParser } from '../../core/PageInteraction/PageParser'
import { LinkName } from '../../helpers/constants/LinkName'
import { ContainerTypes } from '../../helpers/ContainerValues'
import delay from '../../helpers/functions/delay'
import { NextPipeline } from '../../helpers/Pipeline'
import Logger = pino.Logger

interface WaitUntilRefreshButtonDisappearParams {
  timeoutInMs?: number
  linkAppearance?: LinkName[]
  onFail?: () => Promise<void>
  refreshInterval?: number
}

export function WaitUntilRefreshButtonDisappear(args: WaitUntilRefreshButtonDisappearParams) {
  const options: Required<WaitUntilRefreshButtonDisappearParams> = Object.assign(
    {
      timeoutInMs: 0,
      linkAppearance: [],
      onFail: async () => undefined,
      refreshInterval: 5000,
    },
    args,
  )
  const logger = container.get<Logger>(ContainerTypes.LOGGER)

  return async (carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> => {
    logger.info('Waiting on refresh button disappear...')
    const startWaitTime = Date.now()
    let links = await PageParser.getLinks(carry.page, 'a')
    while (links[LinkName.REFRESH]) {
      if (options.timeoutInMs && Date.now() - startWaitTime > options.timeoutInMs) {
        await options.onFail()
        logger.info('Timeout reached while waiting refresh button poof')
        return carry
      }

      await delay(options.refreshInterval)
      await followLink(carry.page, links[LinkName.REFRESH].href)
      links = await PageParser.getLinks(carry.page, 'a')
      if (options.linkAppearance.find((linkName) => links[linkName])) {
        break
      }
    }
    logger.info('Done waiting for that refresh button')

    return next(carry)
  }
}
