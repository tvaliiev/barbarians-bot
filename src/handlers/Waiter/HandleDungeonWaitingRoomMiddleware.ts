import { inject } from 'inversify'
import { provide } from 'inversify-binding-decorators'
import pino from 'pino'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { followLink } from '../../core/PageInteraction/Navigation'
import { PageParser } from '../../core/PageInteraction/PageParser'
import { LinkName } from '../../helpers/constants/LinkName'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { NextPipeline } from '../../helpers/Pipeline'
import { Storage } from '../../helpers/Storage'
import { HandlerContract } from '../HandlerContract'
import Logger = pino.Logger

@provide(HandleDungeonWaitingRoomMiddleware)
export class HandleDungeonWaitingRoomMiddleware implements HandlerContract<FighterBotCarry> {
  constructor(
    private storage: Storage,
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
  ) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    const dungeonLinks = (await PageParser.parseDungeonsPage(carry.page)).filter((e) => !!e.href)

    if (!dungeonLinks.length) {
      return next(carry)
    }

    if (typeof dungeonLinks[0].href !== 'string') {
      throw new Error()
    }

    let link
    for (let i = dungeonLinks.length - 1; i >= 0; i--) {
      const currenLink = dungeonLinks[i]
      await followLink(carry.page, currenLink.href as string)
      const pageData = await PageParser.parseDungeonRoomPage(carry.page)

      if (!pageData) {
        throw new Error()
      }

      if (pageData.actualDoctors < pageData.neededDoctors * 5) {
        link = currenLink
        break
      }
    }

    if (!link) {
      this.logger.info('Could not find any links in dungeon to follow. Every room is full. Exiting')
      this.storage.postponeDungeonFight()
      return carry
    }

    const links = await PageParser.getLinks(carry.page, 'a')

    if (links[LinkName.JOIN_QUEUE]) {
      await followLink(carry.page, links[LinkName.JOIN_QUEUE].href)
    }

    return next(carry)
  }
}
