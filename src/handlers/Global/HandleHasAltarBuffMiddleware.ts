import { FighterBotCarry } from '../../core/bots/FighterBot'
import { provideSingleton } from '../../helpers/functions/provideSingleton'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'

@provideSingleton(HandleHasAltarBuffMiddleware)
export class HandleHasAltarBuffMiddleware implements HandlerContract<FighterBotCarry> {
  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    await carry.hero.refreshGuildAltarBuffIfNeeded(carry.page)

    return next(carry)
  }
}
