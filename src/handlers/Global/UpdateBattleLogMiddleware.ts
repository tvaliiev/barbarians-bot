import { HandlerContract } from '../HandlerContract'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { NextPipeline } from '../../helpers/Pipeline'
import { provide } from 'inversify-binding-decorators'
import { BattleLogs } from '../../helpers/LogEntries/BattleLogs'

@provide(UpdateBattleLogMiddleware)
export class UpdateBattleLogMiddleware implements HandlerContract<FighterBotCarry> {
  async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    await BattleLogs.updateBattleLogFromPageToHero(carry.page, carry.hero)
    return next(carry)
  }
}
