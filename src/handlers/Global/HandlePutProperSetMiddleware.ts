import { FighterBotCarry } from '../../core/bots/FighterBot'
import { SetNames } from '../../core/PageInteraction/PageParsers/SetsPageParser'
import { NextPipeline } from '../../helpers/Pipeline'

export function HandlePutProperSetMiddleware(neededSet: SetNames) {
  return async (carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> => {
    await carry.hero.chooseSet(carry.page, neededSet)

    return next(carry)
  }
}
