import { inject } from 'inversify'
import { provide } from 'inversify-binding-decorators'
import pino from 'pino'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'
import Logger = pino.Logger

@provide(HandleUpdateHeroGlobalStatsMiddleware)
export class HandleUpdateHeroGlobalStatsMiddleware implements HandlerContract<FighterBotCarry> {
  constructor(
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
  ) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    const previousExperience = carry.hero.experience
    await carry.hero.updateGlobalInfo(carry.page)

    // Hero leveled up
    if (previousExperience > carry.hero.experience) {
      this.logger.info('I leveled up!')
      await carry.hero.onLevelUp(carry.page)
      return carry
    }

    return next(carry)
  }
}
