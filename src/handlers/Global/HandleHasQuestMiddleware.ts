import { injectable } from 'inversify'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'

@injectable()
export class HandleHasQuestMiddleware implements HandlerContract<FighterBotCarry> {
  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    await carry.hero.takeBestQuest(carry.page)
    return next(carry)
  }
}
