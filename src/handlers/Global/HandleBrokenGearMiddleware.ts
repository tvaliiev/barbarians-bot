import { FighterBotCarry } from '../../core/bots/FighterBot'
import { UserBodyPageActions } from '../../core/PageInteraction/PageActions/UserBodyPageActions'
import { provideSingleton } from '../../helpers/functions/provideSingleton'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'

@provideSingleton(HandleBrokenGearMiddleware)
export class HandleBrokenGearMiddleware implements HandlerContract<FighterBotCarry> {
  constructor(private readonly userBodyPageActions: UserBodyPageActions) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    if (!(await this.userBodyPageActions.hasBrokenGear(carry.page))) {
      return next(carry)
    }

    await this.userBodyPageActions.repairGear(carry.page, carry.hero)

    return next(carry)
  }
}
