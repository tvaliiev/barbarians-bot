import { inject } from 'inversify'
import { provide } from 'inversify-binding-decorators'
import pino from 'pino'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { TalentPageActions } from '../../core/PageInteraction/PageActions/TalentPageActions'
import { PageParser } from '../../core/PageInteraction/PageParser'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'
import Logger = pino.Logger

@provide(HandleStuffOnHeroPageMiddleware)
export class HandleStuffOnHeroPageMiddleware implements HandlerContract<FighterBotCarry> {
  constructor(
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
    private talentPageActions: TalentPageActions,
  ) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    const hasNotification = await PageParser.hasUserPageNotifications(carry.page)

    if (!hasNotification) {
      return next(carry)
    }

    this.logger.info('Looks like i have something in profile that needs attention. Heading there')

    const hasUndistributedTalentPoints = await PageParser.hasUndistributedTalentPoints(carry.page)

    if (!hasUndistributedTalentPoints) {
      throw new Error('Dont know how to deal with (+) on my hero page')
    }

    this.logger.info('I have undistributed talent points')
    await this.talentPageActions.spendTalentPoints(carry.page)

    return carry
  }
}
