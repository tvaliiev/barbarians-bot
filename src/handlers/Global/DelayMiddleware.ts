import delay from '../../helpers/functions/delay'
import { NextPipeline } from '../../helpers/Pipeline'

export function delayMiddleware<T>(preDelay: number, postDelay = 0) {
  return async (carry: T, next: NextPipeline<T>): Promise<T> => {
    await delay(preDelay)
    const result = await next(carry)
    await delay(postDelay)
    return result
  }
}
