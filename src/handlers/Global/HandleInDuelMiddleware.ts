import { FighterBotCarry } from '../../core/bots/FighterBot'
import { followLink } from '../../core/PageInteraction/Navigation'
import { PageParser } from '../../core/PageInteraction/PageParser'
import { NextPipeline } from '../../helpers/Pipeline'

export enum DuelType {
  OGRES = 'ogres',
  DUEL = 'duel',
}

const links: { [key in DuelType]: string } = {
  [DuelType.DUEL]: '/game/duel',
  [DuelType.OGRES]: '/game/ogreDungeon',
}

export function HandleInDuelMiddleware(duelType: DuelType) {
  return async (carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>) => {
    await followLink(carry.page, '/')
    await followLink(carry.page, links[duelType])

    if (!(await PageParser.isInDuel(carry.page))) {
      await PageParser.updateDuelAndOgreDungeonAvailability(carry.page)
      return carry
    }

    return next(carry)
  }
}
