import { provide } from 'inversify-binding-decorators'
import pino from 'pino'
import { Page } from 'puppeteer'
import { container } from '../../container'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { PageParser } from '../../core/PageInteraction/PageParser'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { NextPipeline } from '../../helpers/Pipeline'
import { Storage } from '../../helpers/Storage'
import { HandlerContract } from '../HandlerContract'
import Logger = pino.Logger

@provide(UpdateDungeonAvailability)
export class UpdateDungeonAvailability implements HandlerContract<FighterBotCarry> {
  constructor() {}

  public static async updateNextAvailableDungeonTime(page: Page) {
    const dungeonAvailability = await PageParser.parseDungeonsPage(page)

    const closestAvailableVariant = dungeonAvailability.reduce(
      (acc, e) => (e.availableAt < acc.availableAt ? e : acc),
      dungeonAvailability[0],
    )

    const storage = container.get(Storage)
    storage.dungeonAvailableAt = closestAvailableVariant.availableAt
    container
      .get<Logger>(ContainerTypes.LOGGER)
      .info(
        `Next available dungeon fight at ${storage.dungeonAvailableAt} (in ${closestAvailableVariant.relativeTime} seconds)`,
      )
  }

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    await UpdateDungeonAvailability.updateNextAvailableDungeonTime(carry.page)
    return next(carry)
  }
}
