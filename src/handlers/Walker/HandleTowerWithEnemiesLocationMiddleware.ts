import { inject } from 'inversify'
import { provide } from 'inversify-binding-decorators'
import pino from 'pino'
import { Page } from 'puppeteer'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { followLink } from '../../core/PageInteraction/Navigation'
import {
  AdvanceLocationsAlgorithm,
  FightWithCommonLinksPageActions,
} from '../../core/PageInteraction/PageActions/FightWithCommonLinksPageActions'
import { PageParser } from '../../core/PageInteraction/PageParser'
import { LinkName } from '../../helpers/constants/LinkName'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'
import Logger = pino.Logger

@provide(HandleTowerWithEnemiesLocationMiddleware)
export class HandleTowerWithEnemiesLocationMiddleware implements HandlerContract<FighterBotCarry> {
  constructor(
    @inject(ContainerTypes.LOGGER)
    private readonly logger: Logger,
    private readonly pageActions: FightWithCommonLinksPageActions,
  ) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    if (!(await this.isInTowers(carry.page))) {
      await followLink(carry.page, '/game/towers')
    }

    await this.pageActions.advanceLocations(carry, AdvanceLocationsAlgorithm.SIMPLE)

    return next(carry)
  }

  public async isInTowers(page: Page): Promise<boolean> {
    const links = await PageParser.getLinks(page)
    if (links[LinkName.HEAL_ALLIES]) {
      return true
    }

    const link = await page.$('img[src="/images/icons/red_warrior.png"]')
    return link !== null
  }
}
