import pino from 'pino'
import { Page } from 'puppeteer'
import { container } from '../../container'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { Hero } from '../../core/Hero'
import { followLink } from '../../core/PageInteraction/Navigation'
import { PageParser } from '../../core/PageInteraction/PageParser'
import { ClanQuest } from '../../core/PageInteraction/PageParsers/ClanQuestsPageParser'
import { LinkName } from '../../helpers/constants/LinkName'
import { ContainerTypes } from '../../helpers/ContainerValues'
import { NextPipeline } from '../../helpers/Pipeline'
import { Storage } from '../../helpers/Storage'
import Logger = pino.Logger

export enum ArenaType {
  SURVIVAL = 'survival',
  ARENA = 'arena',
}

export function handleInArenaAndJoinedMiddleware(arenaType: ArenaType) {
  const logger = container.get<Logger>(ContainerTypes.LOGGER)
  const storage = container.get(Storage)
  const postponeFunctions: { [key in ArenaType]: (hero: Hero, page: Page) => any } = {
    [ArenaType.SURVIVAL]: async (hero: Hero, page: Page) => {
      storage.postponeSurvivalFight()
      await hero.dismissQuest(ClanQuest.SURVIVAL, page)
    },
    [ArenaType.ARENA]: () => storage.postponeArenaFight(),
  }
  return async (carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> => {
    logger.info(`Joining ${arenaType} fight...`)
    await followLink(carry.page, '/game/' + arenaType)

    const result = await PageParser.followLinkIfExists([LinkName.NEW_FIGHT, LinkName.JOIN_QUEUE], carry.page, 'a')
    if (!result) {
      await postponeFunctions[arenaType](carry.hero, carry.page)
      return carry
    }

    return next(carry)
  }
}
