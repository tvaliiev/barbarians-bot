import { injectable } from 'inversify'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { followLink } from '../../core/PageInteraction/Navigation'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'

@injectable()
export class HandleInBattlefieldsAndJoinedMiddleware implements HandlerContract<FighterBotCarry> {
  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    await followLink(carry.page, '/game/bg')
    return next(carry)
  }
}
