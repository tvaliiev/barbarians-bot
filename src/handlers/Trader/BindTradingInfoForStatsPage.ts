import { inject } from 'inversify'
import { TradingBotCarry } from '../../core/bots/TraderBot'
import { provideSingleton } from '../../helpers/functions/provideSingleton'
import { LiveStats } from '../../helpers/LiveStats'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'

@provideSingleton(BindTradingInfoForStatsPage)
export class BindTradingInfoForStatsPage implements HandlerContract<TradingBotCarry> {
  constructor(
    @inject(LiveStats)
    private liveStats: LiveStats,
  ) {}

  public async handle(carry: TradingBotCarry, next: NextPipeline<TradingBotCarry>): Promise<TradingBotCarry> {
    await this.liveStats.updateTradingOptions(carry.tradingOptions)

    return next(carry)
  }
}
