import { Page } from 'puppeteer'
import { TradingBotCarry, TradingOption, TradingOptionStatType, TradingTypes } from '../../core/bots/TraderBot'
import { followLink } from '../../core/PageInteraction/Navigation'
import { PageParser } from '../../core/PageInteraction/PageParser'
import { provideSingleton } from '../../helpers/functions/provideSingleton'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'

@provideSingleton(InjectTradingInfoMiddleware)
export class InjectTradingInfoMiddleware implements HandlerContract<TradingBotCarry> {
  public async handle(carry: TradingBotCarry, next: NextPipeline<TradingBotCarry>): Promise<TradingBotCarry> {
    const page = carry.mainPage
    await followLink(page, '/trade/bazaar')
    const tradingOptions: { [key in TradingTypes]: TradingOption[] } = {
      [TradingTypes.IRON]: [],
      [TradingTypes.MIFRIL]: [],
      [TradingTypes.POTIONS]: [],
    }

    const configs: { linkPart: string; type: TradingTypes; tabName: string }[] = [
      { linkPart: 'Iron', type: TradingTypes.IRON, tabName: 'Железо' },
      { linkPart: 'Potions', type: TradingTypes.POTIONS, tabName: 'Бутылочки' },
      { linkPart: 'Mifril', type: TradingTypes.MIFRIL, tabName: 'Мифрил' },
    ]
    const statTypeFunctions: { [key in TradingOptionStatType]: (options: TradingOption[]) => null | number } = {
      [TradingOptionStatType.MIN_COST_PER_ITEM]: (options: TradingOption[]) =>
        options.reduce((acc, current) => Math.min(acc, current.costPerItem), options[0].costPerItem),
      [TradingOptionStatType.MAX_COST_PER_ITEM]: (options: TradingOption[]) =>
        options.reduce((acc, current) => Math.max(acc, current.costPerItem), options[0].costPerItem),
      [TradingOptionStatType.AVG_COST_PER_ITEM]: (options: TradingOption[]) =>
        options.reduce((acc, current) => acc + current.costPerItem, 0) / options.length,
    }

    for (let i = 0; i < configs.length; i++) {
      const config = configs[i]
      try {
        await PageParser.bgSafeClick(page, `a[href*="filterMyLotsLink"]`)
      } catch (e) {
        // Nothing
      }

      if (!(await page.$(`a[href*="filter${config.linkPart}Link::ILinkListener"]`))) {
        throw new Error(`Cannot find any ${config.linkPart} links for ${config.tabName}`)
      }
      await PageParser.bgSafeClick(page, `a[href*="filter${config.linkPart}Link::ILinkListener"]`)

      let currentPage = 1
      while (await page.$('a[href*="paginator:container:next"]')) {
        const currentPageOptions = await this.getCurrentPageOptions(page, config, currentPage)
        tradingOptions[config.type].push(...currentPageOptions)
        await PageParser.bgSafeClick(page, 'a[href*="paginator:container:next"]')
        currentPage++
      }
      // We are on last page and want to do final parsing
      const currentPageOptions = await this.getCurrentPageOptions(page, config, currentPage)
      tradingOptions[config.type].push(...currentPageOptions)

      Object.keys(statTypeFunctions).forEach((key) => {
        if (typeof carry.tradingOptions[config.type] !== 'object') {
          //@ts-ignore
          carry.tradingOptions[config.type] = {}
        }
        //@ts-ignore
        carry.tradingOptions[config.type][key] = statTypeFunctions[key](tradingOptions[config.type])
      })
    }

    return next(carry)
  }

  private async getCurrentPageOptions(
    mainPage: Page,
    config: { linkPart: string; type: TradingTypes; tabName: string },
    currentPage: number,
  ) {
    return await mainPage.evaluate(
      (config, currentPage) => {
        const buyLinkElements = Array.from(document.querySelectorAll(`a[href*="buyLink"], a[href*="cancelLink"]`))

        return buyLinkElements.map((element) => {
          const amount = parseInt(element.parentElement?.children[1].textContent || '')
          const costElementChildren = Array.from(element.parentElement?.children[3].children || [])
          let cost = 0
          for (let i = 0; i < costElementChildren.length; i += 2) {
            const isGold =
              // @ts-ignore
              costElementChildren[i].getAttribute('src').indexOf('money_gold') !== -1
            const amount = parseInt(costElementChildren[i + 1].textContent || '')
            cost += amount * (isGold ? 100 : 1)
          }

          if (amount === 0 || cost === 0) {
            throw new Error('Cannot parse cost and amount from row')
          }

          const href = element.getAttribute('href')
          if (!href) {
            throw new Error('cannot parse href of link')
          }
          return {
            href,
            type: config.type,
            costTotal: cost,
            amount,
            page: currentPage,
            slotNumber: parseInt(Array.from(href.matchAll(/slots:(\d+)/g))[0][1]),
            costPerItem: (cost / amount) * (config.type === 'iron' ? 1000 : 1),
            isMyBid: !!element.textContent?.match(/отменить/g),
          }
        })
      },
      config,
      currentPage,
    )
  }
}
