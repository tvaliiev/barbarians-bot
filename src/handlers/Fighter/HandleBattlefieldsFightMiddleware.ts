import { injectable } from 'inversify'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import {
  AdvanceLocationsAlgorithm,
  FighterMode,
  FightWithCommonLinksPageActions,
  WaitAfterAttackMode,
} from '../../core/PageInteraction/PageActions/FightWithCommonLinksPageActions'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'

@injectable()
export class HandleBattlefieldsFightMiddleware implements HandlerContract<FighterBotCarry> {
  constructor(private readonly fightWithCommonLinksActions: FightWithCommonLinksPageActions) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    await this.fightWithCommonLinksActions.handleFight(carry, {
      shouldDrinkPotions: true,
      fighterMode: FighterMode.FIGHTER,
      advanceLocationsAlg: AdvanceLocationsAlgorithm.SIMPLE,
      waitAfterAttackMode: WaitAfterAttackMode.PVP_WITH_GLOBAL,
    })

    return next(carry)
  }
}
