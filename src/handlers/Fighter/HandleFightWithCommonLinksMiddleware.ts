import { provide } from 'inversify-binding-decorators'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import {
  AdvanceLocationsAlgorithm,
  FighterMode,
  FightWithCommonLinksPageActions,
  WaitAfterAttackMode,
} from '../../core/PageInteraction/PageActions/FightWithCommonLinksPageActions'
import { NextPipeline } from '../../helpers/Pipeline'
import { HandlerContract } from '../HandlerContract'

@provide(HandleFightWithCommonLinksMiddleware)
export class HandleFightWithCommonLinksMiddleware implements HandlerContract<FighterBotCarry> {
  constructor(private readonly pageActions: FightWithCommonLinksPageActions) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    await this.pageActions.handleFight(carry, {
      advanceLocationsAlg: AdvanceLocationsAlgorithm.NONE,
      fighterMode: FighterMode.FIGHTER,
      shouldDrinkPotions: true,
      waitAfterAttackMode: WaitAfterAttackMode.PVP,
    })

    return next(carry)
  }
}
