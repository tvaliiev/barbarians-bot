import { inject } from 'inversify'
import pino from 'pino'
import { FighterBotCarry } from '../../core/bots/FighterBot'
import { followLink } from '../../core/PageInteraction/Navigation'
import { Link, PageParser } from '../../core/PageInteraction/PageParser'
import { ContainerTypes } from '../../helpers/ContainerValues'
import delay from '../../helpers/functions/delay'
import { provideSingleton } from '../../helpers/functions/provideSingleton'
import { LiveStats } from '../../helpers/LiveStats'
import { NextPipeline } from '../../helpers/Pipeline'
import { Storage } from '../../helpers/Storage'
import { HandlerContract } from '../HandlerContract'
import Logger = pino.Logger

@provideSingleton(HandleFightWithDuelTypeLinksMiddleware)
export class HandleFightWithDuelTypeLinksMiddleware implements HandlerContract<FighterBotCarry> {
  constructor(
    private storage: Storage,
    @inject(ContainerTypes.LOGGER)
    private logger: Logger,
    private readonly liveStats: LiveStats,
  ) {}

  public async handle(carry: FighterBotCarry, next: NextPipeline<FighterBotCarry>): Promise<FighterBotCarry> {
    const fightStartTime = Date.now()
    this.logger.info('Fight started...')

    while (await PageParser.isInDuel(carry.page)) {
      await carry.hero.updateGlobalInfo(carry.page)
      const parsedPage = await PageParser.parseFightDuelTypePage(carry.page)
      let chosenLink: Link | undefined = parsedPage.abilities
        .reverse()
        .find((ability) => ability.active === 0 && ability.cooldown === 0)
      if (!chosenLink) {
        chosenLink = parsedPage.attackLastTarget
      }

      if ((await carry.hero.shouldUsePotion()) && parsedPage.potion && parsedPage.potion.left > 1) {
        chosenLink = parsedPage.potion
      }

      await followLink(carry.page, chosenLink.href)
      await this.liveStats.logDamage(carry.page)
      await delay(this.storage.actionDelayWithGlobal)
    }

    this.logger.info(`Fight ended in ${(Date.now() - fightStartTime) / 1000}`)

    return next(carry)
  }
}
