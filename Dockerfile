FROM node:21.0.0

ENV BROWSER_PATH "/usr/bin/google-chrome"

RUN apt-get update && apt-get install curl gnupg -y \
  && curl --location --silent https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
  && apt-get update \
  && apt-get install google-chrome-stable -y --no-install-recommends \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*


ADD --chown=1000:1000 . /app

WORKDIR /app

EXPOSE 2020

USER 1000:1000
ENTRYPOINT node lib/main.js

